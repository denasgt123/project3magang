/*!
 * Dark Mode Switch v1.0.1 (https://github.com/coliff/dark-mode-switch)
 * Copyright 2021 C.Oliff
 * Licensed under MIT (https://github.com/coliff/dark-mode-switch/blob/main/LICENSE)
 */
// Livewire.on('darkModeInit', () => {
	let darkSwitch = $('body > #darkSwitch');
	let darkSwitch2 = $('body > #darkSwitch2');
	let darkThemeSelected;
	$(document).ready(function () {
		initTheme();
		// darkSwitch.on('click', function (event) {
		// 	// initTheme();
		// 	changeTheme();
		// });
		$('body').on('click', '#darkSwitch', function (event) {
			// initTheme();
			changeTheme();
		});

		$('body').on('click', '#darkSwitch2', function (event) {
			// initTheme();
			changeTheme();
		});
	});
	// let darkSwitch = document.getElementById("darkSwitch");
	// window.addEventListener("load", function () {
	//   if (darkSwitch) {
	//     initTheme();
	//     darkSwitch.addEventListener("change", function () {
	//       resetTheme();
	//     });
	//   }
	// });

	/**
	 * Summary: function that adds or removes the attribute 'data-theme' depending if
	 * the switch is 'on' or 'off'.
	 *
	 * Description: initTheme is a function that uses localStorage from JavaScript DOM,
	 * to store the value of the HTML switch. If the switch was already switched to
	 * 'on' it will set an HTML attribute to the body named: 'data-theme' to a 'dark'
	 * value. If it is the first time opening the page, or if the switch was off the
	 * 'data-theme' attribute will not be set.
	 * @return {void}
	 */
	function initTheme() {
		darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem("darkSwitch") === "dark";
		// console.log(darkThemeSelected);
		if(darkThemeSelected) {
			$('body').attr("data-theme", "dark");
			$('body').find('#darkSwitch').html('<img src="./img/icons/After/sun-solid.svg" height="24" alt="icon" class="pb-1"> <h6 class="mb-0">Light</h6>');
			$('body').find('#darkSwitch2').html('<div class="col-1"><img src="./img/icons/After/sun-solid.svg" height="16" alt="icon" class="me-3"></div><span class="align-middle">Light Mode</span>');
		} else {
			$('body').removeAttr("data-theme");
			$('body').find('#darkSwitch').html('<img src="./img/icons/After/moon-solid.svg" height="24" alt="icon" class="pb-1"> <h6 class="mb-0">Dark</h6>');
			$('body').find('#darkSwitch2').html('<div class="col-1"><img src="./img/icons/After/moon-solid.svg" height="16" alt="icon" class="me-3"></div><span class="align-middle">Dark Mode</span>');
		}
	}
	// function initTheme() {
	// 	var darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem("darkSwitch") === "dark";
	//   	darkSwitch.checked = darkThemeSelected;
	//   	darkThemeSelected ? document.body.setAttribute("data-theme", "dark") : document.body.removeAttribute("data-theme");
	// }

	/**
	 * Summary: resetTheme checks if the switch is 'on' or 'off' and if it is toggled
	 * on it will set the HTML attribute 'data-theme' to dark so the dark-theme CSS is
	 * applied.
	 * @return {void}
	 */
	function changeTheme() {
		darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem("darkSwitch") === "dark";
		if(darkThemeSelected) {
			localStorage.removeItem("darkSwitch");
			initTheme();
		} else {
			localStorage.setItem("darkSwitch", "dark");
			initTheme();
		}
	}

	// function resetTheme() {
	// 	if (darkSwitch.checked) {
	// 		document.body.setAttribute("data-theme", "dark");
	// 		localStorage.setItem("darkSwitch", "dark");
	// 	} else {
	// 		document.body.removeAttribute("data-theme");
	// 		localStorage.removeItem("darkSwitch");
	// 	}
	// }
// });