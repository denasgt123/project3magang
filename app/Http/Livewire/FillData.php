<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class FillData extends Component
{
	use LivewireAlert;

	public $gender, $location, $phone, $username, $password, $password_confirmation;

	public function rules()
	{
		return [
			'username' 	=> ['regex:/^\S*$/u', 'unique:users', 'nullable'],
			'password' 	=> ['confirmed', 'nullable'],
			'phone'		=> ['numeric', 'nullable'],
		];
	}

	public function mount()
	{
		// Init data for input
		$this->gender = 'male';
		$this->location = 'surabaya';
	}

	public function render()
	{
		return view('livewire.fill-data')->extends('layouts.app')->section('content');
	}

	public function checkInput()
	{
		$this->validate();
	}

	public function fillData()
	{
		$this->validate([
			'gender' 	=> 'required',
			'location' 	=> 'required',
			'phone' 	=> ['required', 'numeric'],
			'username' 	=> ['required', 'regex:/^\S*$/u', 'unique:users'],
			'password' 	=> ['required', 'confirmed'],
		]);

		// Get user data from database
		$user = User::find(Auth::id());

		// FIll the data and edit the data in database
		$user->fill([
			'username'	=> $this->username,
			'phone'		=> $this->phone,
			'gender'	=> $this->gender,
			'location'	=> $this->location,
			'password'	=> $this->password,
		]);

		$user->save();

		// Give success message after edit profile finish
		$this->alert('success', "Success!", [
			'toast'    	=> false,
			'timer' 	=> 10000,
			'text'     	=> 'Thank you for completing the registration, please enjoy our beautiful feature !!',
			'position' 	=> 'center',

		]);

		return redirect()->route('home');
	}
}
