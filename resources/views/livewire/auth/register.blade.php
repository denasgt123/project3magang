@section('style')
    {{-- CSS Style for Login Page --}}
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

<div class="container">
    <div class="d-flex align-items-center justify-content-center min-vh-100 w-100">
        <div class="col-xl-7 col-lg-6 col-12 px-5 d-none d-lg-block ">
            <img class="w-100 my-5 pe-5" src="{{ asset('img/1x1 plus tulisan.png') }}" alt="Logo Hitch.Ink">
        </div>
        <div class="col-xl-5 col-lg-6 col-12">
            <div class="w-100 my-5 d-flex justify-content-center align-items-center d-lg-none">
                <img src="{{ asset('img/logo.png') }}" alt="Logo Hitch.Ink" style="height: 10rem; width: auto;">
            </div>

            <div class="card border-0 bg-main neumorph-light" style="border-radius: 1rem;">
                <div class="card-body px-5">
                    <form wire:submit.prevent="registerUser">
                        @csrf
                        <h2 class="mb-sm-5 mb-5 mt-sm-4 mt-4 fw-bold">Sign Up</h2>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                id="InputName" placeholder="name@example.com" value="{{ old('name') }}" autofocus
                                required autocomplete="name" wire:model.defer="name">
                            <label for="InputName">Full Name <span class="text-danger">*</span></label>

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <select class="form-select @error('gender') is-invalid @enderror"
                                aria-label="Default select example" wire:model="gender" id="InputGender" required
                                autocomplete="gender">
                                <option value="male" selected>Male</option>
                                <option value="female">Female</option>
                            </select>
                            <label for="InputGender">Gender <span class="text-danger">*</span></label>

                            @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                id="InputPhone" placeholder="08xxxxxxxxx" value="{{ old('phone') }}" required
                                autocomplete="name" wire:model.defer="phone" wire:change='checkInput' pattern="[0-9]">
                            <label for="InputPhone">Phone Number <span class="text-danger">*</span></label>

                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <select class="form-select @error('location') is-invalid @enderror"
                                aria-label="Default select example" wire:model="location" id="InputLocation"
                                required autocomplete="location">
                                <option value="surabaya" selected>Surabaya</option>
                                <option value="jakarta">Jakarta</option>
                            </select>
                            <label for="InputLocation">Location <span class="text-danger">*</span></label>

                            @error('location')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="email" class="form-control @error('email') is-invalid @enderror"
                                id="InputEmail" placeholder="name@example.com" value="{{ old('email') }}" required
                                autocomplete="email" wire:model.defer="email" wire:change='checkInput'>
                            <label for="InputEmail">Email address <span class="text-danger">*</span></label>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="text" class="form-control @error('username') is-invalid @enderror"
                                id="InputUsername" placeholder="my_User-name123" value="{{ old('username') }}"
                                autofocus required autocomplete="username" wire:model.defer="username"
                                wire:change='checkInput'>
                            <label for="InputUsername">Username <span class="text-danger">*</span></label>

                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                id="InputPassword" placeholder="*******" required autocomplete="current-password"
                                wire:model.defer="password">
                            <label for="InputPassword">Password <span class="text-danger">*</span></label>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="password" class="form-control" id="InputConfirmPassword"
                                name="password_confirmation" placeholder="*******" required
                                autocomplete="new-password" wire:model.defer='password_confirmation'
                                wire:change='checkInput'>
                            <label for="InputConfirmPassword">Confirm Password <span
                                    class="text-danger">*</span></label>
                        </div>

                        <p class="text-center my-4">Already have an account?
                            <a data-toggle="tab" href="{{ route('login') }}">Sign In</a>
                        </p>
                        <button class="btn btn-primary btn-lg px-5 w-100 rounded mb-3" type="submit">Sign up
                            now</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
