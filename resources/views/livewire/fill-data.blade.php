@section('style')
    {{-- CSS Style for Login Page --}}
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

<div class="container" wire:poll>
    <div class="d-flex align-items-center justify-content-center min-vh-100 w-100">
        <div class="col-xl-7 col-lg-6 col-12 px-5 d-none d-lg-block">
            <div class="d-flex justify-content-center align-items-center w-100">
                <img class="my-3" src="{{ asset('img/logo.png') }}" alt="Logo Hitch.Ink"
                    style="height: auto; width: auto; max-height: 400px;">
            </div>
            <h1 class="text-center mt-4 mb-0" style="font-size: 3rem;"><span class="text-green">Thank</span> <span
                    class="text-blue">You</span></h1>
            <h5 class="text-center fw-normal mb-4 fs-5">for registering to our apllication</h5>
            <div class="text-center mt-3">Before you can access our beatifull feature, please provide the following
                data or just
                <a class="text-decoration-none text-danger lh-1" href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out.
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </div>
        <div class="col-xl-5 col-lg-6 col-12">
            <div class="w-100 my-4 d-flex justify-content-center align-items-center flex-wrap d-lg-none">
                <div class="w-100 d-flex justify-content-center align-items-center">
                    <img src="{{ asset('img/logo.png') }}" alt="Logo Hitch.Ink" style="height: 10rem; width: auto;">
                </div>
                <h1 class="w-100 text-center mt-3 mb-0" style="font-size: 2.5rem;"><span class="text-green">Thank</span>
                    <span class="text-blue">You</span>
                </h1>
                <h5 class="text-center fw-normal mb-3 fs-5">for registering to our apllication</h5>
                <div class="text-center mb-3">Before you can access our beatifull feature, please provide the following
                    data or just
                    <a class="text-decoration-none text-danger lh-1" href="{{ route('logout') }}"
                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out.
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>

            <div class="card border-0 bg-main neumorph-light" style="border-radius: 1rem;">
                <div class="card-body px-5">
                    <form wire:submit.prevent="fillData">
                        @csrf
                        <h2 class="mb-sm-5 mb-5 mt-sm-4 mt-4 fw-bold">Complete Registration</h2>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="text" class="form-control @error('username') is-invalid @enderror"
                                id="InputUsername" placeholder="my_User-name123" value="{{ old('username') }}" autofocus
                                required autocomplete="username" wire:model.defer="username" wire:change='checkInput'>
                            <label for="InputUsername">Username <span class="text-danger">*</span></label>

                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <select class="form-select @error('gender') is-invalid @enderror"
                                aria-label="Default select example" wire:model.defer="gender" id="InputGender" required
                                autocomplete="gender">
                                <option value="male" selected>Male</option>
                                <option value="female">Female</option>
                            </select>
                            <label for="InputGender">Gender <span class="text-danger">*</span></label>

                            @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="text" class="form-control @error('phone') is-invalid @enderror"
                                id="floatingInput" placeholder="name@example.com" value="{{ old('phone') }}" autofocus
                                required autocomplete="phone" wire:model.defer="phone" wire:change='checkInput'
                                pattern="[0-9]">
                            <label for="floatingInput">Phone number <span class="text-danger">*</span></label>

                            @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <select class="form-select @error('location') is-invalid @enderror"
                                aria-label="Default select example" wire:model.defer="location" id="InputLocation"
                                required autocomplete="location">
                                <option value="surabaya" selected>Surabaya</option>
                                <option value="jakarta">Jakarta</option>
                            </select>
                            <label for="InputLocation">Location <span class="text-danger">*</span></label>

                            @error('location')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                id="floatingPassword" placeholder="*******" required autocomplete="current-password"
                                wire:model.defer="password">
                            <label for="floatingPassword">Password <span class="text-danger">*</span></label>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="password" class="form-control" id="InputConfirmPassword"
                                name="password_confirmation" placeholder="*******" required
                                autocomplete="new-password" wire:model.defer='password_confirmation'
                                wire:change='checkInput'>
                            <label for="InputConfirmPassword">Confirm Password <span
                                    class="text-danger">*</span></label>
                        </div>
                        <button class="btn btn-primary btn-lg px-5 w-100 rounded" type="submit">Save data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script>
        $(document).ready(function() {
            // Start giving value darkmode from js to php livewire
            darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                "darkSwitch") === "dark";
            if (darkThemeSelected) {
                Livewire.emit('darkMode', 1)
            } else {
                Livewire.emit('darkMode', 0)
            }
            Livewire.on("darkModeInit", () => {
                darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                    "darkSwitch") === "dark";
                if (darkThemeSelected) {
                    Livewire.emit('darkMode', 1)
                } else {
                    Livewire.emit('darkMode', 0)
                }
            });
            // End giving value darkmode from js to php livewire
        });
    </script>
@endpush
