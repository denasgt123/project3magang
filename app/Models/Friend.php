<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Friend extends Model
{
	use HasFactory;

	/**
	 * Get the me that owns the Friend
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function me()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function friend()
	{
		return $this->belongsTo(User::class, 'friend_id');
	}
}
