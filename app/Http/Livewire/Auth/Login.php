<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\RateLimiter;
use Laravel\Socialite\Facades\Socialite;
use Livewire\Component;

class Login extends Component
{
	public $email, $username, $password, $remember;
	public function render()
	{
		return view('livewire.auth.login')->extends('layouts.app')->section('content');
	}

	public function loginUser()
	{
		$this->validate([
			'email' => ['required'],
			'password' => ['required', 'string'],
		]);

		$throttleKey = strtolower($this->email) . '|' . request()->ip();

		if (RateLimiter::tooManyAttempts($throttleKey, 5)) {
			$this->addError('email', __('auth.throttle', [
				'seconds' => RateLimiter::availableIn($throttleKey)
			]));

			return null;
		}

		if (!Auth::guard()->attempt($this->credentials($this->email), $this->remember)) {
			RateLimiter::hit($throttleKey);

			$this->addError('email', __('auth.failed'));
			return null;
		}

		return redirect()->route('home');
	}

	public function loginGoogleRedirect()
	{
		return Socialite::driver('google')->redirect();
	}

	public function loginGoogleCallback()
	{
		try {
			$user_google    = Socialite::driver('google')->user();
			$user           = User::where('email', $user_google->getEmail())->first();

			// jika user ada maka langsung di redirect ke halaman home
			// jika user tidak ada maka simpan ke database
			// $user_google menyimpan data google account seperti email, foto, dsb
			// dd($user);
			if ($user) {
				Auth::login($user);
				// return redirect()->route('profile', Auth::user()->username);
				return redirect()->route('home');
			} else {
				$create = User::Create([
					'email'             => $user_google->getEmail(),
					'name'              => $user_google->getName(),
					'password'          => bcrypt('password'),
					'created_at' 		=> Carbon::now(),
					'updated_at' 		=> Carbon::now(),
					'email_verified_at' => Carbon::now()
				]);
				$create->username = "user" . $create->id;
				$create->save();
				Auth::login($create);
				return redirect()->route('fill-data');
			}
		} catch (Exception $e) {
			return redirect()->route('login');
		}
	}

	private function credentials($username)
	{
		$login = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
		if ($login == 'username') {
			$this->username = $username;
		}

		return $this->only([$login, 'password']);
	}

	public function checkEmail()
	{
		$this->validate([
			'email' => ['required'],
		]);
	}
}
