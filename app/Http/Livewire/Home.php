<?php

namespace App\Http\Livewire;

use App\Models\ChatRoom;
use App\Models\Friend;
use App\Models\User;
use App\Http\Traits\UploadFile;
use App\Models\Chat;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Livewire\Component;
use Livewire\WithFileUploads;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class Home extends Component
{
	use LivewireAlert;
	use UploadFile;
	use WithFileUploads;

	public $side_menu = 1, $others, $friend_requests, $pending_requests, $friends, $signed_user, $other_user, $is_friend, $is_friend_request, $is_request_friend, $is_friend_blocked, $chat_rooms, $interlocuter, $search, $search_gender, $search_location, $blacklisted_ids, $new_msg_count, $new_request_count;

	public $chats = [];

	public $chat_input, $media_chat;

	public $room;

	// for saving darkmode status in php 
	public $dark;

	// for storing profile's input
	public $photo, $name, $username, $bio, $email, $phone, $gender, $location;

	// for storing profile's input
	public $old_password, $new_password, $new_password_confirmation;

	protected $listeners = [
		'editProfile'
	];
	public function getListeners()
	{
		return ['editProfile'];
	}

	// function livewire 
	public function mount()
	{
		// Init logged in user data
		$this->initProfileData();

		if ($this->signed_user->phone == null || $this->signed_user->phone == '') {
			return redirect()->route("fill-data");
		}

		// Init default left side menu open
		$this->changeMenu(1);

		// Refreshing and clear data chat
		$this->refreshChat();
	}

	public function hydrate()
	{
		// Calling js code for checking darkmode status 
		$this->emit('darkModeInit');
	}

	public function render()
	{
		// dd($this->bio);
		if ($this->room && !$this->is_friend_blocked) {
			$this->room->chats()->whereNot('sender_id', Auth::id())->where('deleted_from_receiver', 0)->update([
				'is_seen' => 1,
			]);
		}
		$is_search_name = ($this->search && $this->search != null && $this->search != '') ? true : false;
		$is_search_gender = ($this->search_gender && $this->search_gender != null && $this->search_gender != '') ? true : false;
		$is_search_location = ($this->search_location && $this->search_location != null && $this->search_location != '') ? true : false;
		// dd($is_search_name);
		// dd($is_search_gender);
		// dd($is_search_location);

		if ($this->other_user && $this->other_user != null) {
			$this->getProfile($this->other_user->id);
			if ($this->is_friend) {
				$this->refreshChat();
			}
		}

		// if ($this->chats && $this->chats != null) {
		// 	$this->openRoomIfExist($this->other_user->id);
		// }

		$this->initListOthersFriendsChatsData(1);

		if (!$is_search_name && !$is_search_gender && !$is_search_location) {
			if ($this->side_menu != 3) {
				$this->initListOthersFriendsChatsData(3);
			} else {
				$this->initListOthersFriendsChatsData($this->side_menu);
			}
		}

		return view('livewire.home')->extends('layouts.app')->section('content');
	}

	public function updatedSearch($value)
	{
		if (!$value || $value == '' || $value == null) {
			$value = null;
		}
		if ($this->search_gender && $this->search_gender != '' && $this->search_gender != null) {
			if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
				$this->search($value, $this->search_gender, $this->search_location);
			} else {
				$this->search($value, $this->search_gender);
			}
		} else {
			if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
				$this->search($value, null, $this->search_location);
			} else {
				$this->search($value);
			}
		}
	}

	public function updatedSearchGender($value)
	{
		if (!$value || $value == '' || $value == null) {
			$value = null;
		}
		if ($this->search && $this->search != '' && $this->search != null) {
			if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
				$this->search($this->search, $value, $this->search_location);
			} else {
				$this->search($this->search, $value);
			}
		} else {
			if ($this->search_location && $this->search_location != '' && $this->search_location != null) {
				$this->search(null, $value, $this->search_location);
			} else {
				$this->search(null, $value);
			}
		}
	}

	public function updatedSearchLocation($value)
	{
		if (!$value || $value == '' || $value == null) {
			$value = null;
		}
		if ($this->search && $this->search != '' && $this->search != null) {
			if ($this->search_gender && $this->search_gender != '' && $this->search_gender != null) {
				$this->search($this->search, $this->search_gender, $value);
			} else {
				$this->search($this->search, null, $value);
			}
		} else {
			if ($this->search_gender && $this->search_gender != '' && $this->search_gender != null) {
				$this->search(null, $this->search_gender, $value);
			} else {
				$this->search(null, null, $value);
			}
		}
	}

	public function updatedMediaChat($value)
	{
		$this->room = ChatRoom::hasUser($this->signed_user->id)->hasUser($this->other_user->id)->first();
		$result = '';

		if (!$this->room) {
			$this->room = ChatRoom::create([
				'name' => "{$this->signed_user->name} to {$this->other_user->name}"
			]);
			$this->room->users()->sync([$this->signed_user->id, $this->other_user->id]);
		}

		if ($value && $value != '' && !is_string($value)) {

			$extension = $value->getClientOriginalExtension();
			if ($extension == 'png' || $extension == 'jpg') {
				$result = $this->_uploadFile($value, 'img/chat');
			} elseif ($extension == 'mp4') {
				$result = $this->_uploadFile($value, 'vid/chat');
			} else {
				$result = $this->_uploadFile($value, 'doc/chat');
			}

			if ($result['path']) {
				if ($this->other_user->acceptedFriends->where('id', $this->signed_user->id)->first()->acceptedFriend->is_blocked == 1) {
					$this->room->chats()->create([
						'sender_id' 			=> $this->signed_user->id,
						'is_file'   			=> 1,
						'mime_type'   			=> $result['extension'],
						'file_size'   			=> $result['file_size'],
						'content'   			=> $result['filename'],
						'deleted_from_receiver'	=> 1,
					]);
				} else {
					$this->room->chats()->create([
						'sender_id' 			=> $this->signed_user->id,
						'is_file'   			=> 1,
						'mime_type'   			=> $result['extension'],
						'file_size'   			=> $result['file_size'],
						'content'   			=> $result['filename'],
					]);
				}
			}
		}
		// dd($value);
	}

	// function from this component
	public function changeMenu($menu = null)
	{
		$this->clearSearchUser();
		if ($menu != null) {
			// Changing the menu and get all data need for that menu
			$this->side_menu = $menu;
			$this->initListOthersFriendsChatsData($menu);
		} else {
			$friend = $this->signed_user->acceptedFriends;
			if ($friend->count() > 0) {
				$this->side_menu = 2;
				$this->initListOthersFriendsChatsData(2);
			} else {
				$this->side_menu = 3;
				$this->initListOthersFriendsChatsData(3);
			}
		}
	}

	public function darkMode($status = null)
	{
		// Set the darkmode value in php
		if ($status == null) {
			$this->dark = 0;
		} else {
			$this->dark = $status;
		}
	}

	public function getProfile($user_id)
	{
		$this->reset(['is_friend', 'is_friend_blocked', 'is_request_friend', 'is_friend_request']);
		$friend = $this->signed_user->acceptedFriends()->withTrashed()->where('friend_id', $user_id)->first();
		$request_friend = $this->signed_user->requestedFriends()->withTrashed()->where('friend_id', $user_id)->first();
		$friend_request = $this->signed_user->friendsRequested()->withTrashed()->where('user_id', $user_id)->first();
		// dd($friend);
		// Get detail profile data of other user for right side menu
		if ($friend) {
			$this->is_friend = true;
			if ($friend->acceptedFriend->is_blocked == 1) {
				$this->is_friend_blocked = true;
			} else {
				$this->is_friend_blocked = false;
			}
		} elseif ($request_friend) {
			$this->is_friend = false;
			$this->is_request_friend = true;
			$this->is_friend_blocked = false;
		} elseif ($friend_request) {
			$this->is_friend = false;
			$this->is_friend_request = true;
			$this->is_friend_blocked = false;
		} else {
			$this->is_friend = false;
			$this->is_friend_blocked = false;
		}

		$this->other_user = User::withTrashed()->whereId($user_id)->first();
	}

	public function addFriend(User $user)
	{
		// dd('addfriend');
		// Get data that user if he already add me as friend
		$potentialFriend = $this->signed_user->friendsRequested->where('id', $user->id)->first();
		$accepted        = 0;
		$msg             = 'Friend request sent successfully!';
		$this->is_friend = false;
		$this->is_request_friend = true;

		// if that user already send friend request
		if ($potentialFriend) {
			$potentialFriend->friends->where('id', $this->signed_user->id)->first()->friend->is_accepted = 1;
			$potentialFriend->friends->where('id', $this->signed_user->id)->first()->friend->save();

			$accepted = 1;
			$msg      = "$user->name has become your friend!";
			$this->is_friend = true;
			$this->is_request_friend = false;
		}

		// Get data logged in user if he already sent friend requrst
		$requestedFriend = $this->signed_user->requestedFriends()->where('friend_id', $user->id)->first();

		// if logged in user doesn't send friend request before
		if (!isset($requestedFriend)) {
			$this->signed_user->friends()->attach($user->id, ['is_asked' => 1, 'is_accepted' => $accepted]);
		}

		// Send message success if friend request finish
		$this->alert('success', "Success!", [
			'toast'    => false,
			'text'     => $msg,
			'position' => 'center',
		]);

		// Re-render list other user and friend data
		$this->initListOthersFriendsChatsData(3);
		// $this->getProfile($user);
	}

	public function cancelAddFriend($user_id)
	{
		$user = User::withTrashed()->find($user_id);

		// dd($this->signed_user->friends()->whereFriendId($user->id)->get());
		$this->signed_user->friends()->detach($user_id);
		// $this->getProfile($user);
		$this->is_request_friend = false;
		$this->alert('success', 'Sucees!', [
			'text' => "You are canceled friend request with $user->name!"
		]);
	}

	public function unFriend($user_id)
	{
		$user = User::withTrashed()->find($user_id);

		// dd($this->room->id);
		$this->signed_user->friends()->detach($user->id);
		$user->friends()->detach($this->signed_user->id);

		if (!is_null($user->deleted_at)) {
			// dd($this->signed_user->rooms()->where('id', $this->room->id));
			$this->signed_user->rooms()->detach($this->room->id);
			$user->rooms()->detach($this->room->id);
			// $room = ChatRoom::find($this->room->id);
			// $room->delete();
			$this->other_user = null;
			// $this->initListOthersFriendsChatsData(1);
		}

		// $this->getProfile($user);
		$this->is_friend = false;
		$this->alert('success', 'Sucees!', [
			'text' => "You are no longer friends with $user->name!"
		]);
	}

	public function blockFriend(User $user)
	{
		$this->signed_user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->is_blocked = 1;
		$this->signed_user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->save();
		$this->is_friend_blocked = true;
		$this->alert('success', 'Sucees!', [
			'text' => "You are now block this friend!"
		]);
	}

	public function unblockFriend(User $user)
	{
		$this->signed_user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->is_blocked = 0;
		$this->signed_user->acceptedFriends->where('id', $user->id)->first()->acceptedFriend->save();
		$this->is_friend_blocked = false;
		$this->alert('success', 'Sucees!', [
			'text' => "You are now unblock this friend!"
		]);
	}

	public function initProfileData($is_all_data = true)
	{
		// Get logged in user data
		$this->signed_user = User::where('id', Auth::id())->first();

		if ($is_all_data) {
			// Fill the varible for edit profile form with logged in user data 
			$this->photo     = $this->signed_user->photo;
			$this->name     = $this->signed_user->name;
			$this->username = $this->signed_user->username;
			$this->email    = $this->signed_user->email;
			$this->phone    = $this->signed_user->phone;
			$this->gender    = $this->signed_user->gender;
			$this->location    = $this->signed_user->location;
			$this->bio      = $this->signed_user->bio;
		}

		$this->emit('initJsEmailUserData');
	}

	public function initListOthersFriendsChatsData($menu)
	{
		// Check which menu is rendering
		if ($menu === 1) {
			// Get all list chat rooms for list Chats
			// join('chats', '', '=', )
			// $this->chat_rooms = ChatRoom::select('chat_rooms.*')->join('chats', 'chat_rooms.id', '=', 'chats.room_id')->with('participants.user')->hasUser($this->signed_user->id)->orderBy('chats.created_at', 'desc')->get();
			// dd($this->chat_rooms);
			// Channel::with('latestReply')->get()->sortByDesc('latestReply.created_at');
			$this->new_msg_count = 0;
			$this->chat_rooms = ChatRoom::with(['participants.user', 'latestChat'])->hasUser($this->signed_user->id)->get()->sortByDesc('latestChat.created_at');
			foreach ($this->chat_rooms as $key => $room) {
				// $unseen_msgs = ;
				$this->chat_rooms[$key]->unseen_msg_count = $room->chats()->whereNot('sender_id', Auth::id())->where('is_seen', 0)->where('deleted_from_receiver', 0)->count();
				$this->new_msg_count += $this->chat_rooms[$key]->unseen_msg_count;
			}
			// dd($chat_rooms);
		} elseif ($menu === 2) {
			// Get all list friend user for Friends
			$this->friends = $this->signed_user->acceptedFriends()->withTrashed()->get();
			// dd($this->signed_user->acceptedFriends);
		} elseif ($menu === 3) {
			// Get all list user where that user send friend request
			$this->friend_requests = $this->signed_user->friendsRequested;
			// dd($this->signed_user->friendsRequested->count());
			$this->new_request_count = $this->signed_user->friendsRequested->count();

			// Get all list user where this user send friend request
			$this->pending_requests = $this->signed_user->requestedFriends;

			// Get all user id where user is not have any relation with logged in user
			$request_friend_ids		= $this->signed_user->requestedFriends()->pluck('friend_id')->toArray();
			$friend_request_ids 	= $this->signed_user->friendsRequested()->pluck('user_id')->toArray();
			$friend_ids         	= $this->signed_user->acceptedFriends()->pluck('friend_id')->toArray();
			$this->blacklisted_ids	= array_merge($request_friend_ids, $friend_request_ids, $friend_ids, [Auth::id()]);

			// Get all list user where that user doesn't have any relation with logged in user
			$this->others = User::whereNotIn('id', $this->blacklisted_ids)->get();
		}
	}

	public function checkEditedEmail()
	{
		// Validating input from edit profile 
		$this->validate([
			'name'     => 'required',
			'username' => ['required', Rule::unique('users')->ignore($this->signed_user->id)],
			'email'    => ['required', Rule::unique('users')->ignore($this->signed_user->id), 'email:rfc,dns'],
			'phone'    => ['required', 'numeric'],
			'gender'   => 'required',
			'location' => 'required',
			'bio'      => 'nullable',
			'photo'    => 'nullable',
		]);

		// Get user data from database
		// $user          = User::find($this->signed_user->id);
		// $current_email = $user->email;
		if ($this->signed_user->email != $this->email) {
			$this->confirm('Before Saving...', [
				'text'            		=> "System detected that you want to change your email. You will need to re-verify Your new email in order to use our app again. Are You sure?",
				'position'        		=> 'center',
				'toast'           		=> false,
				'inputAttributes' 		=> ['need_verify' => 1],
				'confirmButtonColor'	=> '#3085d6',
				'cancelButtonColor'		=> '#d33',
				'confirmButtonText' 	=> 'Yes, Change It!',
				'cancelButtonText'		=> 'No, Cancel It!',
				'onConfirmed'			=> 'editProfile',
			]);
		} else {
			$data['data']['inputAttributes']['need_verify'] = 0;
			$this->editProfile($data);
		}
	}

	public function editProfile($data)
	{
		$is_name_change = $this->signed_user->name != $this->name ? true : false;
		$is_username_change = $this->signed_user->username != $this->username ? true : false;
		$is_email_change = $this->signed_user->email != $this->email ? true : false;
		$is_phone_change = $this->signed_user->phone != $this->phone ? true : false;
		$is_gender_change = $this->signed_user->gender != $this->gender ? true : false;
		$is_location_change = $this->signed_user->location != $this->location ? true : false;
		// $is_bio_change = $this->bio == null || $this->bio == '' ? false : ($this->signed_user->bio != $this->bio ? true : false);
		$is_bio_change = ($this->signed_user->bio != $this->bio ? true : false);
		$is_photo_change = $this->signed_user->photo != $this->photo ? true : false;
		// dd($is_bio_change);
		$is_change = $is_name_change || $is_username_change || $is_email_change || $is_phone_change || $is_gender_change || $is_location_change || $is_bio_change || $is_photo_change ? true : false;

		if ($is_change) {
			$user = User::find($this->signed_user->id);
			// FIll the data and edit the data in database
			$user->fill([
				'name'     => $this->name,
				'username' => $this->username,
				'email'    => $this->email,
				'phone'    => $this->phone,
				'gender'   => $this->gender,
				'location' => $this->location,
				'bio'      => $this->bio,
			]);

			// Check if user upload a foto
			if ($this->photo && !is_string($this->photo)) {
				$result = $this->_uploadFile($this->photo, 'img/user');

				if ($result['path']) {
					$user->fill([
						'photo' => $result['filename'],
					]);
				}
			}

			// Check if user editing email
			// if($is_email_change) {
			// 	$user->email_verified_at = null;
			// 	$user->save();
			// 	return redirect()->route('verification.notice', );
			// }

			if ($data['data']['inputAttributes']['need_verify']) {
				$user->email_verified_at = null;
				$user->save();
				$this->flash('success', 'Success!', [
					'text' => 'Successfully changed profile!'
				], '/');
			}

			$user->save();

			// Give success message after edit profile finish
			$this->alert('success', 'Success!', [
				'text' => 'Successfully changed profile!'
			]);
		}

		// Call js to close edit profile modal
		$this->emit('closeEditProfileModal');

		// Re-init the logged in user data
		$this->initProfileData();
	}

	// public function editEmailConfirm()
	// {
	// 	if ($this->signed_user->email != $this->email) {
	// 		$this->confirm('Are you sure?', [
	// 			'text' => "Want to change your current email with a new email? you will be redirected to re-verify your email before you can access this app again!!",
	// 			'icon' => 'warning',
	// 			'showCancelButton' => true,
	// 			'confirmButtonColor' => '#3085d6',
	// 			'cancelButtonColor' => '#d33',
	// 			'confirmButtonText' => 'Yes, Change It!',
	// 			'cancelButtonText' => 'No, Cancel It!',
	// 			'onConfirmed' => 'editProfile',
	// 		]);
	// 	} else {
	// 		$this->editProfile();
	// 	}
	// }

	public function removePhotoProfile()
	{
		$this->signed_user->photo = 'user.png';
		$this->signed_user->save();

		$this->alert('success', 'Success!', [
			'text' => 'Your profile photo has been successfully removed!'
		]);

		$this->emit('changePreviewPhotoProfile');

		$this->initProfileData();
	}

	public function changePassword()
	{
		# Validation
		$this->validate([
			'old_password' => 'required',
			'new_password' => 'required|confirmed',
		]);

		# Match The Old Password
		if (!Hash::check($this->old_password, auth()->user()->password)) {
			throw ValidationException::withMessages(['old_password' => "Old Password doesn't match!"]);
			// return back()->with("error", "Old Password Doesn't match!");
		}

		#Update the new Password
		User::whereId(auth()->user()->id)->update([
			'password' => Hash::make($this->new_password)
		]);

		// Give success message after change password finish
		$this->alert('success', 'Success!', [
			'text' => 'Successfully changed password!'
		]);

		// reset input change password form data
		$this->reset(['old_password', 'new_password', 'new_password_confirmation']);

		// Call js to close change password modal
		$this->emit('closeChangePasswordModal');
	}

	public function deleteAccount()
	{
		$user = User::find(Auth::id());

		Auth::logout();

		request()->session()->invalidate();

		request()->session()->regenerateToken();

		$user->delete();

		$this->flash('success', 'Berhasil!', [
			'text' => 'Profile Telah Dihapus!'
		], route('login'));
	}

	public function submitChat()
	{
		$this->room = ChatRoom::hasUser($this->signed_user->id)->hasUser($this->other_user->id)->first();

		if (!$this->room) {
			$this->room = ChatRoom::create([
				'name' => "{$this->signed_user->name} to {$this->other_user->name}"
			]);
			$this->room->users()->sync([$this->signed_user->id, $this->other_user->id]);
		}

		if ($this->chat_input && $this->chat_input != '' && $this->chat_input != null) {
			if ($this->other_user->acceptedFriends->where('id', $this->signed_user->id)->first()->acceptedFriend->is_blocked == 1) {
				$this->room->chats()->create([
					'sender_id' 			=> $this->signed_user->id,
					'content'   			=> $this->chat_input,
					'deleted_from_receiver'	=> 1,
				]);
			} else {
				$this->room->chats()->create([
					'sender_id' => $this->signed_user->id,
					'content'   => $this->chat_input,
				]);
			}

			$this->reset('chat_input');
		}

		$this->refreshChat();
	}

	public function unsendChat(Chat $chat)
	{
		if ($chat->sender_id == $this->signed_user->id) {
			$chat->deleted_for_receiver = 1;
		}
		$chat->save();
		$this->alert('success', 'Sucees!', [
			'text' => "The message has been successfully unsend!"
		]);
	}

	public function deleteChat(Chat $chat)
	{
		if ($chat->sender_id == $this->signed_user->id) {
			$chat->deleted_from_sender = 1;
		} else {
			$chat->deleted_from_receiver = 1;
		}
		$chat->save();

		$this->alert('success', 'Sucees!', [
			'text' => "The message has been successfully deleted!"
		]);
	}

	public function deleteAllChat()
	{
		if ($this->room) {
			foreach ($this->room->chats as $chat) {
				if ($chat->sender_id == $this->signed_user->id) {
					$chat->deleted_from_sender = 1;
				} else {
					$chat->deleted_from_receiver = 1;
				}
				$chat->save();
			}
		}

		$this->alert('success', 'Sucees!', [
			'text' => "All message history has been deleted!"
		]);
	}

	public function deleteChatRoom($user_id)
	{
		$user = User::withTrashed()->find($user_id);

		// if (!is_null($user->deleted_at)) {
		// dd($this->signed_user->rooms()->where('id', $this->room->id));
		$this->signed_user->rooms()->detach($this->room->id);
		$user->rooms()->detach($this->room->id);
		// $room = ChatRoom::find($this->room->id);
		// $room->delete();
		$this->other_user = null;
		// }

		$this->alert('success', 'Sucees!', [
			'text' => "All message history has been deleted!"
		]);
	}

	public function openRoom(ChatRoom $room, $interlocuter_id)
	{
		$this->room = $room;
		$this->interlocuter = User::withTrashed()->find($interlocuter_id);
		// dd($this->interlocuter);

		$this->getProfile($interlocuter_id);

		if (!$this->is_friend_blocked) {
			$room->chats()->whereNot('sender_id', Auth::id())->where('deleted_from_receiver', 0)->update([
				'is_seen' => 1,
			]);
		}

		// $interlocuter = $room->participants->where('user_id', '!=', Auth::id())->first()->user;

		// $this->changeActiveChat($interlocuter_id);
		$this->emit('openChatModal');
		$this->refreshChat();
	}

	public function openRoomIfExist($friend_id)
	{
		$this->reset('room');
		$room = User::find(Auth::id())->rooms()->whereHas('participants', function ($q) use ($friend_id) {
			$q->where('user_id', $friend_id);
		})->first();

		if ($room) {
			$this->openRoom($room, $friend_id);
			$this->changeMenu(1);
		} else {
			// $this->changeActiveChat($friend_id);
			$this->getProfile($friend_id);
		}
		$this->refreshChat();
	}

	public function refreshChat()
	{
		if ($this->room) {
			$this->reset(['chats']);
			$this->chats = $this->room->chats;
		}
	}

	public function search($value = null, $gender = null, $location = null)
	{
		$search = ($value && $value != '' && $value != null) ? true : false;
		$search1 = ($gender && $gender != '' && $gender != null) ? true : false;
		$search2 = ($location && $location != '' && $location != null) ? true : false;
		// dd($search, $search1, $search2);

		if ($this->side_menu == 2) {
			if ($search && $search1 && $search2) {
				$friends = $this->signed_user->acceptedFriends()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->where('location', $location)->get();
			} elseif ($search && $search1 && !$search2) {
				$friends = $this->signed_user->acceptedFriends()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->get();
			} elseif ($search && !$search1 && $search2) {
				$friends = $this->signed_user->acceptedFriends()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('location', $location)->get();
			} elseif (!$search && $search1 && $search2) {
				$friends = $this->signed_user->acceptedFriends()->where('gender', $gender)->where('location', $location)->get();
			} elseif (!$search && $search1 && !$search2) {
				$friends = $this->signed_user->acceptedFriends()->where('gender', $gender)->get();
			} elseif (!$search && !$search1 && $search2) {
				$friends = $this->signed_user->acceptedFriends()->where('location', $location)->get();
			} else {
				$friends = $this->signed_user->acceptedFriends()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->get();
			}
			$this->friends = $friends;
		} elseif ($this->side_menu == 3) {
			if ($search && $search1 && $search2) {
				$others = User::where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
				$friend_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->where('location', $location)->get();
				$pending_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->where('location', $location)->get();
			} elseif ($search && $search1 && !$search2) {
				$others = User::where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->whereNotIn('id', $this->blacklisted_ids)->get();
				$friend_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->get();
				$pending_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('gender', $gender)->get();
			} elseif ($search && !$search1 && $search2) {
				$others = User::where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
				$friend_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('location', $location)->get();
				$pending_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->where('location', $location)->get();
			} elseif (!$search && $search1 && $search2) {
				$others = User::where('gender', $gender)->where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
				$friend_requests = $this->signed_user->friendsRequested()->where('gender', $gender)->where('location', $location)->get();
				$pending_requests = $this->signed_user->friendsRequested()->where('gender', $gender)->where('location', $location)->get();
			} elseif (!$search && $search1 && !$search2) {
				$others = User::where('gender', $gender)->whereNotIn('id', $this->blacklisted_ids)->get();

				$friend_requests = $this->signed_user->friendsRequested()->where('gender', $gender)->get();
				$pending_requests = $this->signed_user->friendsRequested()->where('gender', $gender)->get();
			} elseif (!$search && !$search1 && $search2) {
				$others = User::where('location', $location)->whereNotIn('id', $this->blacklisted_ids)->get();
				$friend_requests = $this->signed_user->friendsRequested()->where('location', $location)->get();
				$pending_requests = $this->signed_user->friendsRequested()->where('location', $location)->get();
			} else {
				$others = User::where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->whereNotIn('id', $this->blacklisted_ids)->get();
				$friend_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->get();
				$pending_requests = $this->signed_user->friendsRequested()->where(function ($query) use ($value) {
					$query->where('name', 'like', "%" . $value . "%")->orWhere('username', 'like', "%" . $value . "%");
				})->get();
			}
			$this->pending_requests = $pending_requests;
			$this->friend_requests = $friend_requests;
			$this->others = $others;
			// dd($this->others = $others);
		}
	}

	public function clearSearchUser()
	{
		if ($this->side_menu == 2) {
			$this->initListOthersFriendsChatsData(2);
		} elseif ($this->side_menu == 3) {
			$this->initListOthersFriendsChatsData(3);
		}
		$this->reset('search');
		$this->reset('search_gender');
		$this->reset('search_location');
	}

	public function logout()
	{
		Auth::logout();
		return redirect()->route('login');
	}
}
