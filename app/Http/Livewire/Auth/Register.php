<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;

class Register extends Component
{
	use WithFileUploads;

	public $name, $gender, $location, $phone, $username, $email, $password, $password_confirmation;
	public function render()
	{
		return view('livewire.auth.register')->extends('layouts.app')->section('content');
	}

	public function rules()
	{
		return [
			'username' 	=> ['regex:/^\S*$/u', 'unique:users', 'nullable'],
			'email' 	=> ['email', 'unique:users', 'nullable'],
			'password' 	=> ['confirmed', 'nullable'],
			'phone'		=> ['numeric', 'nullable']
		];
	}

	public function registerUser()
	{
		$this->validate([
			'name' => 'required',
			'gender' => 'required',
			'location' => 'required',
			'phone' => ['required', 'numeric'],
			'username' => ['required', 'regex:/^\S*$/u', 'unique:users'],
			'email' => ['required', 'email', 'unique:users'],
			'password' => ['required', 'confirmed'],
		]);

		$user = User::create([
			'name' => $this->name,
			'gender' => $this->gender,
			'location' => $this->location,
			'phone' => $this->phone,
			'username' => $this->username,
			'email' => $this->email,
			'password' => bcrypt($this->password),
		]);

		Auth::guard()->login($user);
		event(new Registered($user));
		return redirect()->route('home');
	}

	public function checkInput()
	{
		$this->validate();
	}

	// public function checkEmail()
	// {
	// 	$this->validate();
	// }

	// public function checkUsername()
	// {
	// 	$this->validate();
	// }

	// public function checkPasswordConfirmation()
	// {
	// 	$this->validate();
	// }
}
