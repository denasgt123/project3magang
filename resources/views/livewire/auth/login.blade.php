@section('style')
    {{-- CSS Style for Login Page --}}
    <link rel="stylesheet" href="{{ asset('css/login.css') }}">
@endsection

<div class="container">
    <div class="d-flex align-items-center justify-content-center min-vh-100 w-100">
        <div class="col-xl-7 col-lg-6 col-12 px-5 d-none d-lg-block ">
            <img class="w-100 my-5 pe-5" src="{{ asset('img/1x1 plus tulisan.png') }}" alt="Logo Hitch.Ink">
        </div>
        <div class="col-xl-5 col-lg-6 col-12">
            <div class="w-100 my-5 d-flex justify-content-center align-items-center d-lg-none">
                <img src="{{ asset('img/logo.png') }}" alt="Logo Hitch.Ink" style="height: 10rem; width: auto;">
            </div>

            <div class="card border-0 bg-main neumorph-light" style="border-radius: 1rem;">
                <div class="card-body px-5">
                    <form wire:submit.prevent="loginUser">
                        @csrf
                        <h2 class="mb-sm-5 mb-5 mt-sm-4 mt-4 fw-bold">Sign in</h2>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="text" class="form-control @error('email') is-invalid @enderror"
                                id="floatingInput" placeholder="name@example.com" value="{{ old('email') }}" autofocus
                                required autocomplete="email" wire:model.defer="email" wire:change='checkEmail'>
                            <label for="floatingInput">Email address or Username</label>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-floating mb-sm-4 mb-3">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                id="floatingPassword" placeholder="*******" required autocomplete="current-password"
                                wire:model.defer="password">
                            <label for="floatingPassword">Password</label>

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <!-- Checkbox -->
                        {{-- <div class="row justify-content-between">
                            <div class="col-12 col-sm-6 py-2 py-sm-0 d-flex align-items-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="rememberme" />
                                    <label class="form-check-label px-2" for="rememberme"
                                        {{ old('remember') ? 'checked' : '' }} wire:model.defer="remember">Remember
                                        Me</label>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 py-2 py-sm-0 text-sm-end text-center">
                                <button class="btn btn-primary btn-md px-5 rounded-pill" type="submit">Login</button>
                            </div>
                        </div> --}}

                        <p class="text-center my-4">Doesn't have an account? <a data-toggle="tab"
                                href="{{ route('register') }}">Sign
                                Up</a></p>
                        <button class="btn btn-primary btn-lg px-5 w-100 rounded" type="submit">Login</button>

                        <div class="row my-3">
                            <div class="col-5 gx-0">
                                <hr>
                            </div>
                            <div class="col-2 gx-0">
                                <p class="text-muted text-center mb-0">Or</p>
                            </div>
                            <div class="col-5 gx-0">
                                <hr>
                            </div>
                        </div>

                        <a href="{{ route('google') }}"
                            class="btn btn-lg w-100 google-btn mb-3 text-decoration-none neumorph-light"
                            style="background-color: #dd4b39; color: white;" type="submit"><i
                                class="fab fa-google"></i> Sign in with
                            google</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script')
    <script>
        $(document).ready(function() {
            // Start giving value darkmode from js to php livewire
            darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                "darkSwitch") === "dark";
            if (darkThemeSelected) {
                Livewire.emit('darkMode', 1)
            } else {
                Livewire.emit('darkMode', 0)
            }
            Livewire.on("darkModeInit", () => {
                darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                    "darkSwitch") === "dark";
                if (darkThemeSelected) {
                    Livewire.emit('darkMode', 1)
                } else {
                    Livewire.emit('darkMode', 0)
                }
            });
            // End giving value darkmode from js to php livewire
        });
    </script>
@endpush
