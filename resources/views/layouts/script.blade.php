{{-- PWA Script Service Worker --}}
<script>
    if (!navigator.serviceWorker.controller) {
        navigator.serviceWorker.register("/sw.js").then(function(reg) {
            console.log("Service worker has been registered for scope: " + reg.scope);
        });
    }
</script>

{{-- Jquerry for Bootstrap CDN --}}
{{-- <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI="
	crossorigin="anonymous"></script> --}}
<script src="{{ asset('js/jquery-3.6.1.js') }}"></script>

{{-- Feather Icon CDN --}}
{{-- <script src="{{ asset('js/feather.min.js') }}"></script> --}}
{{-- <script src="https://unpkg.com/feather-icons"></script> --}}

<!-- Bootstrap Script CDN -->
{{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous">
</script> --}}
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

{{-- AOS Script CDN --}}
{{-- <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> --}}
<script src="{{ asset('js/aos.js') }}"></script>

{{-- Darkmode Script --}}
<script src="{{ asset('js/darkmode.js') }}"></script>

{{-- Swal Script CDN --}}
{{-- <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script> --}}

{{-- Smooth Scrollbar Script --}}
{{-- <script src="{{ asset('js/dist/smooth-scrollbar.js') }}"></script>
<script>
  	var Scrollbar = window.Scrollbar;
//   var options = {
// 	'damping': 0.1,
//   };
	Scrollbar.initAll();
//   Scrollbar.init(document.querySelector('#my-scrollbar'), options);
</script> --}}

{{-- Font Awesome Script --}}
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
{{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> --}}
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}

{{-- Custom Script --}}
{{-- <script src="{{ asset('js/scripthome.jss') }}" defer></script> --}}
