<?php

namespace App\Http\Traits;

// use Illuminate\Support\Facades\Request;
// use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

trait UploadFile
{

	/**
	 * Upload file using storeAs() function. Recommend for saving file in storage's symlink folder.
	 *
	 * @param  Illuminate\Support\Facades\Request::File $file - the uploaded file (e.g., $request->thumbnail)
	 * @param  string $where - path you want to put the file at (e.g., "public/photos")
	 * @param  string $identifier - custom identifier, adding custom string in front of the filename, required if this function is called twice in the same code to prevent generating duplicate filenames. (example, adding "thumbnail" will make generated file name like "thumbnail_filename_time().jpg")
	 * @return array [$path, $savedFileName]
	 */
	protected static function _uploadFile($file, $where, $identifier = null)
	{
		$filenameWithExt	= $file->getClientOriginalName();
		$filename       	= Str::slug(pathinfo($filenameWithExt, PATHINFO_FILENAME));
		$extension      	= $file->getClientOriginalExtension();
		$file_size			= $file->getSize();

		if ($identifier) {
			$savedFileName = $identifier . '_' . $filename . '_' . time()  . '.' . $extension;
		} else {
			$savedFileName = $filename . '_' . time()  . '.' . $extension;
		}
		$path = $file->storeAs($where, $savedFileName, ['disk' => 'public_path']);

		// dd($extension);
		// if ($extension == 'png' || $extension == 'jpg') {
		// 	$type = 'image';
		// } else if ($extension == 'mp4') {
		// 	$type = 'video';
		// } else {
		// 	$type = 'file';
		// }

		return ["path" => $path, "filename" => $savedFileName, "extension" => $extension, "file_size" => $file_size];
		// $path = Storage::disk('local')->put($where.'/'.$filenamSeSimpan, fopen($file, 'r+'));
	}
}
