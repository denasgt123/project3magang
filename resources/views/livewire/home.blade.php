@push('style')
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
@endpush

{{-- @section('modal')
    <div class="modal fade" id="imageModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
            <div class="modal-content overflow-hidden">
                <div class="modal-header d-flex justify-content-end border-0">
                    <a href="javascript::void(0)" class="h-100 text-decoration-none me-1" data-bs-dismiss="modal"
                        aria-label="Close"><i class="fa-solid fa-xmark text-dark fs-5"></i></a>
                </div>
                <div class="modal-body p-0 d-flex align-items-center justify-content-center">
                    <img src="" alt="" class="w-100 h-100" style="object-fit: contain">
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="videoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
            <div class="modal-content overflow-hidden">
                <div class="modal-header d-flex justify-content-end border-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body d-flex p-0">
                    <video class="w-100 h-100" style="object-fit: contain" controls>
                        <source src="">
                        Your browser does not support HTML video.
                    </video>
                </div>
            </div>
        </div>
    </div>
@endsection --}}
<div>
    <div class="modal-section">
        {{-- Start chat modal --}}
        @if (isset($other_user))
            {{-- Start active chat modal --}}
            <div class="modal fade" wire:ignore.self id="activeChatMobile" tabindex="-1" aria-labelledby="editProfileModal"
                aria-hidden="true">
                <div class="modal-dialog modal-fullscreen modal-dialog-centered">
                    <div class="modal-content">
                        <div class="d-flex flex-column h-100 w-100">

                            <!-- Chat Header -->
                            <div class="d-flex sticky-top p-3 bg-light2 shadow-sm border border-top-0 border-bottom-0">
                                <div class="d-flex align-items-center">
                                    <a href="javascript:void(0)" class="text-decoration-none me-3"
                                        data-bs-dismiss="modal" aria-label="Close">
                                        <i class="fa-solid fa-arrow-left text-dark fs-6"></i>
                                    </a>
                                </div>
                                <div class="d-flex align-items-center flex-grow-1 open-profile"
                                    style="height: 45px; cursor: pointer;">
                                    <div class="rounded-circle overflow-hidden me-3">
                                        <img src="{{ asset("img/user/$other_user->photo") }}" alt="avatar"
                                            height="40" width="40" style="object-fit:cover;" />
                                    </div>
                                    <h6 class="mb-0">{{ $other_user->name }}</h6>
                                </div>
                                <div class="d-flex align-items-center">
                                    <div class="dropdown">
                                        <button class="btn btn-sm text-dark" type="button" data-bs-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false" wire:ignore.self>
                                            <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end"
                                            aria-labelledby="chat-header-actions" wire:ignore.self>
                                            <a class="dropdown-item open-profile" href="javascript:void(0)"
                                                data-bs-target="#detailProfileMobile">View
                                                Profile</a>
                                            <a class="dropdown-item delete-all-chats" href="javascript:void(0)">Clear
                                                All
                                                Chat</a>
                                            @if ($is_friend === true)
                                                <a class="dropdown-item unfriend-btn" data-id="{{ $other_user->id }}"
                                                    data-name="{{ $other_user->name }}"
                                                    href="javascript:void(0)">Unfriend</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ Chat Header -->

                            <!-- Active Chat -->
                            <div class="active-chat p-3 flex-grow-1 custom-scrollbar" style="overflow-y: auto">
                                <!-- User Chat messages -->
                                <div class="user-chats">
                                    {{-- wire:key="chat-{{ $other_user->id }}" --}}
                                    @forelse ($chats as $chat)
                                        @php
                                            if ($chat->deleted_for_receiver) {
                                                $chat->content = 'Pesan ini telah dihapus!';
                                            }
                                        @endphp
                                        @if ($chat->sender_id == $signed_user->id)
                                            @if ($chat->deleted_from_sender == 1)
                                                @continue
                                            @endif
                                            @if ($chat->is_file && $chat->deleted_for_receiver != 1)
                                                @if ($chat->mime_type == 'png' || $chat->mime_type == 'jpg')
                                                    <div class="chat d-flex flex-wrap flex-row-reverse">
                                                        <div class="chat-section mb-3" style="max-width: 40rem;">
                                                            <div
                                                                class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                                <div class="px-3 pt-3 position-relative mb-1"
                                                                    style="max-width: 20rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #4c49fd 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                    <div class="chat-content text-wrap d-flex justify-content-end"
                                                                        style="min-width: 25px">
                                                                        <img src="{{ asset("img/chat/$chat->content") }}"
                                                                            alt="content"
                                                                            class="w-100 h-100 preview-img"
                                                                            style="object-fit: cover; cursor: pointer; max-height: 500px;"
                                                                            data-bs-target="#imageModal">
                                                                    </div>
                                                                    @if (!$chat->deleted_for_receiver)
                                                                        @if (!$chat->is_seen)
                                                                            <div
                                                                                class="position-absolute bottom-0 start-0">
                                                                                <i class="fa-solid fa-check text-light"
                                                                                    style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                            </div>
                                                                        @else
                                                                            <div
                                                                                class="position-absolute bottom-0 start-0">
                                                                                <i class="fa-solid fa-check-double text-light"
                                                                                    style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                            </div>
                                                                        @endif
                                                                        <div class="position-absolute bottom-0 end-0">
                                                                            <p class="text-light mb-0"
                                                                                style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                                {{ $chat->created_at->format('H:i') }}
                                                                            </p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="dropdown me-1 mt-1">
                                                                    <a class="text-decoration-none text-muted px-2"
                                                                        href="javascript:void(0)" role="button"
                                                                        data-bs-toggle="dropdown" aria-expanded="false"
                                                                        wire:ignore.self>
                                                                        <i
                                                                            class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu" wire:ignore.self>
                                                                        <li class="dropdown-item text-danger unsend-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Unsend
                                                                            Message
                                                                        </li>
                                                                        <li class="dropdown-item text-danger delete-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Delete Message
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif ($chat->mime_type == 'mp4')
                                                    <div class="chat d-flex flex-wrap flex-row-reverse">
                                                        <div class="chat-section mb-3" style="max-width: 40rem;">
                                                            <div
                                                                class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                                <div class="px-3 pt-3 position-relative mb-1"
                                                                    style="max-width: 20rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #3399ff 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                    <div class="chat-content text-wrap position-relative"
                                                                        style="min-width: 15px">
                                                                        <i class="fa-solid fa-video position-absolute ms-2 mt-2 fs-5"
                                                                            style="color: white;"></i>
                                                                        <video class="w-100 h-100 preview-vid"
                                                                            style="object-fit: cover; cursor: pointer; max-height: 500px;"
                                                                            data-bs-target="#videoModal" nocontrols>
                                                                            <source
                                                                                src="{{ asset("vid/chat/$chat->content") }}">
                                                                            Your browser does not support HTML video.
                                                                        </video>
                                                                    </div>
                                                                    @if (!$chat->deleted_for_receiver)
                                                                        <div class="position-absolute bottom-0 end-0">
                                                                            <p class="text-light mb-0"
                                                                                style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                                {{ $chat->created_at->format('H:i') }}
                                                                            </p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="dropdown me-1 mt-1">
                                                                    <a class="text-decoration-none text-muted px-2"
                                                                        href="javascript:void(0)" role="button"
                                                                        data-bs-toggle="dropdown"
                                                                        aria-expanded="false" wire:ignore.self>
                                                                        <i
                                                                            class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu" wire:ignore.self>
                                                                        <li class="dropdown-item text-danger unsend-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Unsend
                                                                            Message
                                                                        </li>
                                                                        <li class="dropdown-item text-danger delete-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Delete Message
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="chat d-flex flex-wrap flex-row-reverse">
                                                        <div class="chat-section mb-3" style="max-width: 40rem;">
                                                            <div
                                                                class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                                <div class="px-3 pt-3 position-relative mb-1"
                                                                    style="max-width: 20rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #3399ff 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                    <div class="chat-content text-wrap d-flex justify-content-end"
                                                                        style="min-width: 15px">
                                                                        <a href="{{ asset("doc/chat/$chat->content") }}"
                                                                            download="{{ $chat->content }}"
                                                                            class="text-decoration-none text-dark mw-100"
                                                                            data-bs-toggle="tooltip"
                                                                            data-bs-title="{{ $chat->content }}">
                                                                            <div class="bg-secondary rounded-1 py-2 px-3 d-flex align-items-center"
                                                                                style="--bs-bg-opacity: .5;">
                                                                                <i
                                                                                    class="fa-solid fa-file-arrow-down fs-5"></i>
                                                                                <div class="ps-3 mw-100">
                                                                                    <h6
                                                                                        class="mb-0 text-normal text-truncate pe-2">
                                                                                        {{ $chat->content }}
                                                                                    </h6>
                                                                                    <p class="mb-0"
                                                                                        style="font-size: .7rem;">
                                                                                        {{ $chat->file_size >= 1000000 ? round($chat->file_size / 1000000) . ' MB' : ($chat->file_size >= 1000 ? round($chat->file_size / 1000) . ' KB' : $chat->file_size . ' Byte') }}
                                                                                        &nbsp;|&nbsp;
                                                                                        {{ strtoupper($chat->mime_type) }}
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                    @if (!$chat->deleted_for_receiver)
                                                                        <div class="position-absolute bottom-0 end-0">
                                                                            <p class="text-light mb-0"
                                                                                style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                                {{ $chat->created_at->format('H:i') }}
                                                                            </p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="dropdown me-1 mt-1">
                                                                    <a class="text-decoration-none text-muted px-2"
                                                                        href="javascript:void(0)" role="button"
                                                                        data-bs-toggle="dropdown"
                                                                        aria-expanded="false" wire:ignore.self>
                                                                        <i
                                                                            class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu" wire:ignore.self>
                                                                        <li class="dropdown-item text-danger unsend-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Unsend
                                                                            Message
                                                                        </li>
                                                                        <li class="dropdown-item text-danger delete-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Delete Message
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="chat d-flex flex-wrap flex-row-reverse">
                                                    <div class="chat-section mb-3" style="max-width: 40rem;">
                                                        <div
                                                            class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                            <div class="px-3 pt-2 position-relative mb-1"
                                                                style="max-width: 20rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #4c49fd 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                <div class="chat-content text-wrap d-flex justify-content-end"
                                                                    style="min-width: 25px">
                                                                    <p class="fs-6 mb-0 lh-sm" style="color: white;">
                                                                        {{ $chat->content }}</p>
                                                                </div>
                                                                @if (!$chat->deleted_for_receiver)
                                                                    @if (!$chat->is_seen)
                                                                        <div
                                                                            class="position-absolute bottom-0 start-0">
                                                                            <i class="fa-solid fa-check text-light"
                                                                                style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                        </div>
                                                                    @else
                                                                        <div
                                                                            class="position-absolute bottom-0 start-0">
                                                                            <i class="fa-solid fa-check-double text-light"
                                                                                style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                        </div>
                                                                    @endif
                                                                    <div class="position-absolute bottom-0 end-0">
                                                                        <p class="text-light mb-0"
                                                                            style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                            {{ $chat->created_at->format('H:i') }}</p>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="dropdown me-1 mt-1">
                                                                <a class="text-decoration-none text-muted px-2"
                                                                    href="javascript:void(0)" role="button"
                                                                    data-bs-toggle="dropdown" aria-expanded="false"
                                                                    wire:ignore.self>
                                                                    <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                </a>
                                                                <ul class="dropdown-menu" wire:ignore.self>
                                                                    <li class="dropdown-item text-danger unsend-msg"
                                                                        style="cursor: pointer;"
                                                                        data-id="{{ $chat->id }}">
                                                                        <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                        Unsend
                                                                        Message
                                                                    </li>
                                                                    <li class="dropdown-item text-danger delete-msg"
                                                                        style="cursor: pointer;"
                                                                        data-id="{{ $chat->id }}">
                                                                        <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                        Delete Message
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            @if ($chat->deleted_from_receiver == 1)
                                                @continue
                                            @endif
                                            @if ($chat->is_file && $chat->deleted_for_receiver != 1)
                                                @if ($chat->mime_type == 'png' || $chat->mime_type == 'jpg')
                                                    <div class="chat d-flex align-items-stretch">
                                                        <div class="chat-section mb-3" style="max-width: 40rem;">
                                                            <div class="chat-bubble d-flex align-items-strecth">
                                                                <div class="px-3 pt-3 mb-1 position-relative border bg-white"
                                                                    style="max-width: 20rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                    <div class="chat-content text-wrap"
                                                                        style="min-width: 15px">
                                                                        <img src="{{ asset("img/chat/$chat->content") }}"
                                                                            alt="content"
                                                                            class="w-100 h-100 preview-img"
                                                                            style="object-fit: cover; cursor: pointer;"
                                                                            data-bs-target="#imageModal">
                                                                    </div>
                                                                    @if (!$chat->deleted_for_receiver)
                                                                        <div class="position-absolute bottom-0 end-0">
                                                                            <p class="text-muted mb-0"
                                                                                style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                                {{ $chat->created_at->format('H:i') }}
                                                                            </p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="dropdown ms-1 mt-1">
                                                                    <a class="text-decoration-none text-muted px-2"
                                                                        href="javascript:void(0)" role="button"
                                                                        data-bs-toggle="dropdown"
                                                                        aria-expanded="false" wire:ignore.self>
                                                                        <i
                                                                            class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu" wire:ignore.self>
                                                                        <li class="dropdown-item text-danger delete-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Delete
                                                                            Message
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif ($chat->mime_type == 'mp4')
                                                    <div class="chat d-flex align-items-stretch">
                                                        <div class="chat-section mb-3" style="max-width: 40rem;">
                                                            <div class="chat-bubble d-flex align-items-strecth">
                                                                <div class="px-3 pt-3 mb-1 position-relative border bg-white"
                                                                    style="max-width: 20rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                    <div class="chat-content text-wrap position-relative"
                                                                        style="min-width: 15px">
                                                                        <i class="fa-solid fa-video position-absolute ms-2 mt-2 fs-6"
                                                                            style="color: white;"></i>
                                                                        <video class="w-100 h-100 preview-vid"
                                                                            style="object-fit: cover; cursor: pointer; max-height: 500px;"
                                                                            data-bs-target="#videoModal" nocontrols>
                                                                            <source
                                                                                src="{{ asset("vid/chat/$chat->content") }}">
                                                                            Your browser does not support HTML video.
                                                                        </video>
                                                                    </div>
                                                                    @if (!$chat->deleted_for_receiver)
                                                                        <div class="position-absolute bottom-0 end-0">
                                                                            <p class="text-muted mb-0"
                                                                                style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                                {{ $chat->created_at->format('H:i') }}
                                                                            </p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="dropdown ms-1 mt-1">
                                                                    <a class="text-decoration-none text-muted px-2"
                                                                        href="javascript:void(0)" role="button"
                                                                        data-bs-toggle="dropdown"
                                                                        aria-expanded="false" wire:ignore.self>
                                                                        <i
                                                                            class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu" wire:ignore.self>
                                                                        <li class="dropdown-item text-danger delete-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Delete
                                                                            Message
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="chat d-flex align-items-stretch">
                                                        <div class="chat-section mb-3" style="max-width: 40rem;">
                                                            <div class="chat-bubble d-flex align-items-strecth">
                                                                <div class="px-3 pt-3 mb-1 position-relative border bg-white"
                                                                    style="max-width: 20rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                    <div class="chat-content text-wrap"
                                                                        style="min-width: 15px">
                                                                        <a href="{{ asset("doc/chat/$chat->content") }}"
                                                                            download="{{ $chat->content }}"
                                                                            class="text-decoration-none text-dark mw-100"
                                                                            data-bs-toggle="tooltip"
                                                                            data-bs-title="{{ $chat->content }}">
                                                                            <div class="bg-secondary rounded-1 py-2 px-3 d-flex align-items-center"
                                                                                style="--bs-bg-opacity: .5;">
                                                                                <i
                                                                                    class="fa-solid fa-file-arrow-down fs-5"></i>
                                                                                <div class="ps-3 mw-100">
                                                                                    <h6
                                                                                        class="mb-0 text-normal text-truncate pe-2">
                                                                                        {{ $chat->content }}
                                                                                    </h6>
                                                                                    <p class="mb-0"
                                                                                        style="font-size: .7rem;">
                                                                                        {{ $chat->file_size >= 1000000 ? round($chat->file_size / 1000000) . ' MB' : ($chat->file_size >= 1000 ? round($chat->file_size / 1000) . ' KB' : $chat->file_size . ' Byte') }}
                                                                                        &nbsp;|&nbsp;
                                                                                        {{ strtoupper($chat->mime_type) }}
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </a>
                                                                    </div>
                                                                    @if (!$chat->deleted_for_receiver)
                                                                        <div class="position-absolute bottom-0 end-0">
                                                                            <p class="text-muted mb-0"
                                                                                style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                                {{ $chat->created_at->format('H:i') }}
                                                                            </p>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="dropdown ms-1 mt-1">
                                                                    <a class="text-decoration-none text-muted px-2"
                                                                        href="javascript:void(0)" role="button"
                                                                        data-bs-toggle="dropdown"
                                                                        aria-expanded="false" wire:ignore.self>
                                                                        <i
                                                                            class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu" wire:ignore.self>
                                                                        <li class="dropdown-item text-danger delete-msg"
                                                                            style="cursor: pointer;"
                                                                            data-id="{{ $chat->id }}">
                                                                            <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                            Delete
                                                                            Message
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="chat d-flex align-items-stretch">
                                                    <div class="chat-section mb-3" style="max-width: 40rem;">
                                                        <div class="chat-bubble d-flex align-items-strecth">
                                                            <div class="px-3 pt-2 mb-1 position-relative border bg-white"
                                                                style="max-width: 20rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                                <div class="chat-content text-wrap"
                                                                    style="min-width: 15px">
                                                                    <p class="fs-6 mb-0 lh-sm">{{ $chat->content }}
                                                                    </p>
                                                                </div>
                                                                @if (!$chat->deleted_for_receiver)
                                                                    <div class="position-absolute bottom-0 end-0">
                                                                        <p class="text-muted mb-0"
                                                                            style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                            {{ $chat->created_at->format('H:i') }}</p>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            <div class="dropdown ms-1 mt-1">
                                                                <a class="text-decoration-none text-muted px-2"
                                                                    href="javascript:void(0)" role="button"
                                                                    data-bs-toggle="dropdown" aria-expanded="false"
                                                                    wire:ignore.self>
                                                                    <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                                </a>
                                                                <ul class="dropdown-menu" wire:ignore.self>
                                                                    <li class="dropdown-item text-danger delete-msg"
                                                                        style="cursor: pointer;"
                                                                        data-id="{{ $chat->id }}">
                                                                        <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                        Delete
                                                                        Message
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    @empty
                                        <div class="text-center mt-5 text-secondary">
                                            You don't have a chat history with this person.
                                        </div>
                                    @endforelse
                                </div>
                                <!-- User Chat messages -->
                            </div>
                            <!--/ Active Chat -->

                            <!-- Submit Chat form -->
                            <div class="chat-app-form sticky-bottom px-3 py-3 bg-light2 shadow-sm">
                                @if ($is_friend == true)
                                    @if (is_null($other_user->deleted_at))
                                        @if (!$is_friend_blocked)
                                            <form action="" method="post" class="input-group"
                                                wire:submit.prevent>
                                                <input type="text" class="form-control border-end-0"
                                                    wire:model="chat_input" id=""
                                                    placeholder="Type your messages here!..." style="resize: none;">
                                                <span class="input-group-text border-start-0 bg-white">
                                                    <a href="javascript:void(0)"
                                                        onclick="this.nextElementSibling.click()">
                                                        <i class="fa-solid fa-file-arrow-down fs-6"></i>
                                                    </a>
                                                    <input class="form-control d-none" type="file" id="mediaChat"
                                                        wire:model="media_chat">
                                                </span>
                                                <button class="btn btn-outline-primary" type="submit"
                                                    id="button-addon2" wire:click.prevent="submitChat()">Send</button>
                                            </form>
                                        @else
                                            <div class="text-center">
                                                <p class="my-2 text-dark">You're blocked this person, unblock first to
                                                    start
                                                    chatting!</p>
                                            </div>
                                        @endif
                                    @else
                                        <div class="text-center">
                                            <p class="my-2 text-dark">Sorry, This account was no longer exist. You
                                                can't send
                                                message to this person again!</p>
                                        </div>
                                    @endif
                                @else
                                    <div class="text-center">
                                        <p class="my-2 text-dark">You must be friend first to exchange
                                            messages with this person.</p>
                                    </div>
                                @endif
                            </div>
                            <!--/ Submit Chat form -->
                        </div>
                    </div>
                </div>
            </div>
            {{-- End active chat modal --}}

            @if ($is_friend == false)
                {{-- Start detail profile modal --}}
                <div class="modal fade" wire:ignore.self id="detailProfileMobile" tabindex="-1"
                    aria-labelledby="editProfileModal" aria-hidden="true">
                    <div class="modal-dialog modal-fullscreen modal-dialog-centered ">
                        <div class="modal-content">
                            <div class="h-100 bg-light2">
                                <div class="d-flex flex-column h-100 w-100">
                                    {{-- Start Others Profile photo, name, and username user --}}
                                    <div
                                        class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                                        <div class="d-flex align-items-center justify-content-between w-100"
                                            style="height: 45px;">
                                            <h4 class="chat-list-title mb-0">Other Profile</h4>
                                            <a href="javascript:void(0)"
                                                class="text-decoration-none btn btn-sm close-profile text-dark"
                                                data-bs-dismiss="modal" aria-label="Close">
                                                <i class="fa-solid fa-xmark fs-5 text-dark"></i>
                                            </a>
                                        </div>
                                    </div>
                                    {{-- End Others Profile photo, name, and username user --}}

                                    {{-- Start Others Profile Bio, Phone, Email, etc --}}
                                    <div class="w-100" data-scrollbar>
                                        <!-- User Profile image with name -->
                                        <header class="d-flex justify-content-center w-100 p-4 bg-white">
                                            <div class="header-profile-sidebar d-flex flex-column align-items-center ">
                                                <div class="d-flex justify-content-center align-items-center rounded-circle border border-2 border-dark overflow-hidden"
                                                    style="width: 150px; height: 150px;">
                                                    <img src="{{ asset("img/user/$other_user->photo") }}"
                                                        alt="user_avatar" class="h-100 w-100 preview-img"
                                                        style="object-fit: cover; cursor: pointer;"
                                                        data-bs-target="#imageModal" />
                                                </div>
                                                <h4 class="chat-user-name mt-4 mb-0">{{ $other_user->name }}
                                                    {!! is_null($other_user->deleted_at)
                                                        ? ($other_user->gender == 'male'
                                                            ? '<i class="fa-solid fa-mars fs-5 text-blue"></i>'
                                                            : '<i class="fa-solid fa-venus fs-5" style="color: #e0276a;"></i>')
                                                        : '' !!}
                                                </h4>
                                                @if (isset($other_user->username))
                                                    <p class="fs-6 text-muted mb-0">
                                                        {{ is_null($other_user->deleted_at) ? "@$other_user->username" : 'This Account was deleted' }}
                                                    </p>
                                                @else
                                                    <p class="fs-6 text-muted mb-0">
                                                        <i>{{ is_null($other_user->deleted_at) ? "This user doesn't have username" : 'This Account was deleted' }}</i>
                                                    </p>
                                                @endif
                                            </div>
                                        </header>
                                        <!--/ User Profile image with name -->

                                        <!-- User Profile information and action -->
                                        @if (is_null($other_user->deleted_at))
                                            <div class="w-100">
                                                <div class="my-2 p-4 bg-white">
                                                    <!-- About User -->
                                                    <h6 class="text-muted fw-bold mb-2">About</h6>
                                                    @if (isset($other_user->bio))
                                                        <p class="fs-6 text-dark mb-0">{{ "$other_user->bio" }}</p>
                                                    @else
                                                        <p class="fs-6 text-muted mb-0"><i>This user doesn't have
                                                                bio</i>
                                                        </p>
                                                    @endif
                                                    <!-- About User -->
                                                </div>

                                                <div class="mt-2 p-4 bg-white">
                                                    <!-- User's Links -->
                                                    <div class="more-options">
                                                        <h6 class="text-muted fw-bold mb-2">Actions</h6>
                                                        <ul class="list-unstyled mb-0">
                                                            <li class="mb-1">
                                                                <a href=""
                                                                    class="d-flex align-items-center text-decoration-none text-success"
                                                                    wire:click="addFriend({{ $other_user->id }})">
                                                                    <i class="col-1 fa-solid fa-user-plus fs-6"></i>
                                                                    <span class="align-middle">Add User as
                                                                        Friend</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <!--/ User's Links -->
                                                </div>

                                            </div>
                                        @else
                                            <div class="my-2 p-4 bg-white text-center">
                                                <h6 class="text-muted fw-bold mb-2">Sorry, this Account was no longer
                                                    exist.</h6>
                                                <a href="javascript:void(0)"
                                                    class="d-flex align-items-center justify-content-center text-decoration-none text-danger unfriend-btn"
                                                    data-id="{{ $other_user->id }}"
                                                    data-name="{{ $other_user->name }}" data-type="leave-chat-room">
                                                    <i class="col-1 fa-solid fa-user-minus fs-6"></i>
                                                    <span class="align-middle">Leave and Delete this chat room.</span>
                                                </a>
                                            </div>
                                        @endif
                                        <!--/ User Profile information and action -->
                                    </div>
                                    {{-- Edit Others Profile Bio, Phone, Email, etc --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End detail profile modal --}}
            @else
                {{-- Start detail profile modal --}}
                <div class="modal fade" wire:ignore.self id="detailProfileMobile" tabindex="-1"
                    aria-labelledby="editProfileModal" aria-hidden="true">
                    <div class="modal-dialog modal-fullscreen modal-dialog-centered ">
                        <div class="modal-content">
                            <!-- Friend profile right area -->
                            <div class="h-100 bg-light2">

                                <!-- Friend profile right area -->
                                <div class="d-flex flex-column h-100 w-100 detail-profile" wire:ignore.self>
                                    <div
                                        class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                                        <div class="d-flex align-items-center justify-content-between w-100"
                                            style="height: 45px;">
                                            <h4 class="chat-list-title mb-0">Friend Profile</h4>
                                            <a href="javascript:void(0)"
                                                class="text-decoration-none btn btn-sm close-profile"
                                                data-bs-dismiss="modal" aria-label="Close"><i
                                                    class="fa-solid fa-xmark fs-5 text-dark"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="w-100 custom-scrollbar" style="overflow-y: auto">
                                        <header class="d-flex justify-content-center w-100 p-4 bg-white">
                                            <!-- User Profile image with name -->
                                            <div class="header-profile-sidebar d-flex flex-column align-items-center ">
                                                <div class="d-flex justify-content-center align-items-center rounded-circle border border-2 border-dark overflow-hidden"
                                                    style="width: 150px; height: 150px;">
                                                    <img src="{{ asset("img/user/$other_user->photo") }}"
                                                        alt="user_avatar" class="h-100 w-100 preview-img"
                                                        style="object-fit: cover; cursor: pointer"
                                                        data-bs-target="#imageModal" />
                                                </div>
                                                <h4 class="chat-user-name mt-4 mb-0">{{ $other_user->name }}</h4>
                                                @if (isset($other_user->username))
                                                    <p class="fs-6 text-muted mb-0">{{ "@$other_user->username" }}</p>
                                                @else
                                                    <p class="fs-6 text-muted mb-0"><i>This user doesn't have
                                                            username</i>
                                                    </p>
                                                @endif
                                            </div>
                                            <!--/ User Profile image with name -->
                                        </header>
                                        <div class="w-100">
                                            <div class="my-2 p-4 bg-white">
                                                <!-- About User -->
                                                <h6 class="text-muted fw-bold mb-2">About</h6>
                                                @if (isset($other_user->bio))
                                                    <p class="fs-6 text-dark mb-0">{{ "$other_user->bio" }}</p>
                                                @else
                                                    <p class="fs-6 text-muted mb-0"><i>This user doesn't have bio</i>
                                                    </p>
                                                @endif
                                                <!-- About User -->
                                            </div>

                                            <div class="my-2 p-4 bg-white">
                                                <!-- User's personal information -->
                                                <div class="personal-info">
                                                    <h6 class="text-muted fw-bold mb-2">Personal Information</h6>
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="mb-1 d-flex align-items-center">
                                                            <div class="col-1">
                                                                <img src="{{ asset('img/icons/After/envelope-solid.svg') }}"
                                                                    height="16" alt="icon" class="me-3">
                                                            </div>
                                                            <span class="align-middle">{{ $other_user->email }}</span>
                                                        </li>
                                                        <li class="mb-1 d-flex align-items-center">
                                                            <div class="col-1">
                                                                <img src="{{ asset('img/icons/After/phone-solid.svg') }}"
                                                                    height="16" alt="icon" class="me-3">
                                                            </div>
                                                            <span class="align-middle">{!! $other_user->phone
                                                                ? $other_user->phone
                                                                : '<i class="fs-6 text-muted">This user doesn`t have phone number</i>' !!}</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!--/ User's personal information -->
                                            </div>

                                            <div class="my-2 p-4 bg-white">
                                                <!-- User's Links -->
                                                <div class="more-options">
                                                    <h6 class="text-muted fw-bold mb-2">Options</h6>
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="mb-1">
                                                            <a href="javascript:void(0)"
                                                                class="row align-items-center text-decoration-none text-dark gx-0 open-profile-media"
                                                                data-menu="document">
                                                                <div class="col-1">
                                                                    <img src="{{ asset('img/icons/After/file-solid.svg') }}"
                                                                        height="16" alt="icon">
                                                                </div>
                                                                <span class="align-middle col-11">Document Files</span>
                                                            </a>
                                                        </li>
                                                        <li class="mb-1">
                                                            <a href="javascript:void(0)"
                                                                class="row align-items-center text-decoration-none text-dark open-profile-media"
                                                                data-menu="photo">
                                                                <div class="col-1">
                                                                    <img src="{{ asset('img/icons/After/images-solid.svg') }}"
                                                                        height="16" alt="icon">
                                                                </div>
                                                                <span class="align-middle col-11">Photo Files</span>
                                                            </a>
                                                        </li>
                                                        <li class="mb-1">
                                                            <a href="javascript:void(0)"
                                                                class="row align-items-center text-decoration-none text-dark open-profile-media"
                                                                data-menu="video">
                                                                <div class="col-1">
                                                                    <img src="{{ asset('img/icons/After/film-solid.svg') }}"
                                                                        height="16" alt="icon">
                                                                </div>
                                                                <span class="align-middle col-11">Video Files</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!--/ User's Links -->
                                            </div>

                                            <div class="mt-2 p-4 bg-white">
                                                <!-- User's Links -->
                                                <div class="more-options">
                                                    <h6 class="text-muted fw-bold mb-2">Actions</h6>
                                                    <ul class="list-unstyled mb-0">
                                                        <li class="mb-1">
                                                            <a href="javascript:void(0)"
                                                                class="d-flex align-items-center text-decoration-none text-danger delete-all-chats">
                                                                <i class="col-1 fa-solid fa-trash fs-6"></i>
                                                                <span class="align-middle">Delete All Messages</span>
                                                            </a>
                                                        </li>
                                                        <li class="mb-1">
                                                            @if (!$is_friend_blocked)
                                                                <a href="javascript:void(0)"
                                                                    class="d-flex align-items-center text-decoration-none text-danger block-btn"
                                                                    data-id="{{ $other_user->id }}">
                                                                    <i class="col-1 fa-solid fa-ban fs-6"></i>
                                                                    <span class="align-middle">Block User</span>
                                                                </a>
                                                            @else
                                                                <a href="javascript:void(0)"
                                                                    class="d-flex align-items-center text-decoration-none text-success unblock-btn"
                                                                    data-id="{{ $other_user->id }}">
                                                                    <i class="col-1 fa-solid fa-lock-open fs-6"></i>
                                                                    <span class="align-middle">Unblock User</span>
                                                            @endif
                                                        </li>
                                                        <li class="mb-1">
                                                            <a href="javascript:void(0)"
                                                                class="d-flex align-items-center text-decoration-none text-danger unfriend-btn"
                                                                data-id="{{ $other_user->id }}"
                                                                data-name="{{ $other_user->name }}">
                                                                <i class="col-1 fa-solid fa-user-minus fs-6"></i>
                                                                <span class="align-middle">Unfriend</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <!--/ User's Links -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/ Friend profile right area -->

                                <!-- Document media right area -->
                                <div class="d-none flex-column h-100 w-100 document-media" wire:ignore.self>
                                    <div
                                        class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                                        <div class="d-flex align-items-center w-100" style="height: 45px;">
                                            <span class="btn btn-sm me-4 close-profile-media"
                                                style="cursor: pointer;"><i
                                                    class="fa-solid fa-arrow-left text-dark  fs-5"></i></span>
                                            <h4 class="chat-list-title mb-0">Document Files</h4>
                                        </div>
                                    </div>
                                    <div class="w-100 custom-scrollbar" style="overflow-y: auto">
                                        <div class="w-100">
                                            @php
                                                $docs = [];
                                                foreach ($chats as $item) {
                                                    if ($item->is_file && ($item->mime_type != 'png' && $item->mime_type != 'jpg' && $item->mime_type != 'mp4')) {
                                                        array_push($docs, $item);
                                                    }
                                                }
                                            @endphp
                                            @forelse ($docs as $item)
                                                <a href="{{ asset("doc/chat/$item->content") }}"
                                                    download="{{ $item->content }}"
                                                    class="text-decoration-none text-dark mw-100"
                                                    data-bs-toggle="tooltip" data-bs-title="{{ $item->content }}">
                                                    <div class="py-2 px-3 m-2 d-flex align-items-center"
                                                        style="--bs-bg-opacity: .5;">
                                                        <i class="fa-solid fa-file-arrow-down fs-5"></i>
                                                        <div class="ps-3 mw-100">
                                                            <h6 class="mb-0 text-normal text-truncate pe-2">
                                                                {{ $item->content }}
                                                            </h6>
                                                            <p class="mb-0" style="font-size: .7rem;">
                                                                {{ $item->file_size >= 1000000 ? round($item->file_size / 1000000) . ' MB' : ($item->file_size >= 1000 ? round($item->file_size / 1000) . ' KB' : $item->file_size . ' Byte') }}
                                                                &nbsp;|&nbsp;
                                                                {{ strtoupper($item->mime_type) }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            @empty
                                                <h6 class="py-5 text-dark text-center w-100">You doesn't have any
                                                    document yet
                                                </h6>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <!--/ Document media right area -->

                                <!-- Photo media right area -->
                                <div class="d-none flex-column h-100 w-100 photo-media" wire:ignore.self>
                                    <div
                                        class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                                        <div class="d-flex align-items-center w-100" style="height: 45px;">
                                            <span class="btn btn-sm me-4 close-profile-media"
                                                style="cursor: pointer;"><i
                                                    class="fa-solid fa-arrow-left text-dark fs-5"></i></span>
                                            <h4 class="chat-list-title mb-0">Photo Files</h4>
                                        </div>
                                    </div>
                                    <div class="w-100 custom-scrollbar" style="overflow-y: auto" data-scrollbar>
                                        <div class="d-flex flex-wrap w-100">
                                            @php
                                                $imgs = [];
                                                foreach ($chats as $item) {
                                                    if ($item->is_file && ($item->mime_type == 'png' || $item->mime_type == 'jpg')) {
                                                        array_push($imgs, $item);
                                                    }
                                                }
                                            @endphp
                                            @forelse ($imgs as $item)
                                                <div class="col-4 p-1 overflow-hidden">
                                                    <img src="{{ asset("img/chat/$item->content") }}" alt="img"
                                                        class="w-100 h-100 preview-img"
                                                        style="object-fit: cover; cursor: pointer;"
                                                        data-bs-target="#imageModal">
                                                </div>
                                            @empty
                                                <h6 class="py-5 text-dark text-center w-100">You doesn't have any photo
                                                    yet
                                                </h6>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <!--/ Photo media right area -->

                                <!-- Video media right area -->
                                <div class="d-none flex-column h-100 w-100 video-media" wire:ignore.self>
                                    <div
                                        class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                                        <div class="d-flex align-items-center w-100" style="height: 45px;">
                                            <span class="btn btn-sm me-4 close-profile-media"
                                                style="cursor: pointer;"><i
                                                    class="fa-solid fa-arrow-left text-dark fs-5"></i></span>
                                            <h4 class="chat-list-title mb-0">Video Files</h4>
                                        </div>
                                    </div>
                                    <div class="w-100 custom-scrollbar" style="overflow-y: auto" data-scrollbar>
                                        <div class="d-flex flex-wrap w-100">
                                            @php
                                                $vids = [];
                                                foreach ($chats as $item) {
                                                    if ($item->is_file && $item->mime_type == 'mp4') {
                                                        array_push($vids, $item);
                                                    }
                                                }
                                            @endphp
                                            @forelse ($vids as $item)
                                                <div class="col-4 p-1 overflow-hidden">
                                                    <video class="w-100 h-100 preview-vid"
                                                        style="object-fit: cover; cursor: pointer;"
                                                        data-bs-target="#videoModal" nocontrols>
                                                        <source src="{{ asset("vid/chat/$item->content") }}">
                                                        Your browser does not support HTML video.
                                                    </video>
                                                </div>
                                            @empty
                                                <h6 class="py-5 text-dark text-center w-100">You doesn't have any photo
                                                    yet
                                                </h6>
                                            @endforelse
                                        </div>
                                    </div>
                                </div>
                                <!--/ Video media right area -->

                            </div>
                            <!--/ Friend profile right area -->
                        </div>
                    </div>
                </div>
                {{-- End detail profile modal --}}
            @endif
        @endif
        {{-- End chat modal --}}

        {{-- Start preview image modal --}}
        <div class="modal fade" wire:ignore id="imageModal" tabindex="-1" aria-labelledby="imageModal"
            aria-hidden="true">
            <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content overflow-hidden">
                    <div class="modal-header d-flex justify-content-end border-0">
                        <a href="javascript::void(0)" class="h-100 text-decoration-none me-1" data-bs-dismiss="modal"
                            aria-label="Close"><i class="fa-solid fa-xmark text-dark fs-5"></i></a>
                    </div>
                    <div class="modal-body p-0 d-flex align-items-center justify-content-center">
                        <img src="" alt="" class="w-100 h-100"
                            style="object-fit: contain; max-height: 700px;">
                    </div>
                </div>
            </div>
        </div>
        {{-- End preview image modal --}}

        {{-- Start preview video modal --}}
        <div class="modal fade" wire:ignore id="videoModal" tabindex="-1" aria-labelledby="videoModal"
            aria-hidden="true">
            <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content overflow-hidden">
                    <div class="modal-header d-flex justify-content-end border-0">
                        <a href="javascript::void(0)" class="h-100 text-decoration-none me-1" data-bs-dismiss="modal"
                            aria-label="Close"><i class="fa-solid fa-xmark text-dark fs-5"></i></a>
                    </div>
                    <div class="modal-body d-flex p-0">
                        <video class="w-100 h-100" style="object-fit: contain; max-height: 700px;" controls>
                            <source src="">
                            Your browser does not support HTML video.
                        </video>
                    </div>
                </div>
            </div>
        </div>
        {{-- Start preview video modal --}}

        {{-- Start edit profile modal --}}
        <div class="modal fade" wire:ignore.self id="editProfileModal" tabindex="-3"
            aria-labelledby="editProfileModal" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered ">
                <div class="modal-content">
                    <form action="">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="editProfileModalLabel">Edit Profile User</h1>
                            <a href="javascript::void(0)" class="h-100 text-decoration-none me-1"
                                data-bs-dismiss="modal" aria-label="Close"><i
                                    class="fa-solid fa-xmark text-dark fs-5"></i></a>
                        </div>
                        <div class="modal-body">
                            <div
                                class="preview-img-container d-flex align-items-center justify-content-center mb-5 mt-3">
                                <div class="position-relative" style="width: 200px; height: 200px;">
                                    <img src="{{ asset("img/user/$signed_user->photo") }}" alt="user_avatar"
                                        class="preview-img-profile rounded-circle overflow-hidden border border-2 border-dark"
                                        style="object-fit: cover; width: 200px; height: 200px;"
                                        data-bs-target="#imageModal" wire:ignore.self />
                                    <span
                                        class="position-absolute translate-middle d-flex align-items-center justify-content-center rounded-circle bg-blue"
                                        style="cursor: pointer; width: 45px; height: 45px; top: 90%; left: 85%; color: white;"
                                        onclick="this.nextElementSibling.click()">
                                        <i class="fa-solid fa-pen fs-5"></i>
                                        <span class="visually-hidden">Edit Photo</span>
                                    </span>
                                    <input class="form-control d-none" type="file" id="photoInput"
                                        wire:model.defer="photo">
                                </div>
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('name') is-invalid @enderror">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                        id="floatingInputName" placeholder="Name" wire:model.defer="name"
                                        maxlength="30">
                                    <label for="floatingInputName">Name <span class="text-danger">*</span></label>
                                </div>
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('username') is-invalid @enderror">
                                    <input type="text" class="form-control @error('username') is-invalid @enderror"
                                        id="floatingInputUsername" placeholder="Username" wire:model.defer="username"
                                        maxlength="30">
                                    <label for="floatingInputUsername">Username <span
                                            class="text-danger">*</span></label>
                                </div>
                                @error('username')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('bio') is-invalid @enderror">
                                    <textarea name="bio" id="floatingInputBio" style="height: 100px;" maxlength="255"
                                        class="w-100 form-control @error('bio') is-invalid @enderror" wire:model.defer="bio"></textarea>
                                    <label for="floatingInputBio">About (Bio)</label>
                                </div>
                                @error('bio')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('gender') is-invalid @enderror">
                                    <select class="w-100 form-select @error('gender') is-invalid @enderror"
                                        aria-label="Select Gender" wire:model="gender" id="InputGender" required>
                                        <option value="male" selected>Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                    <label for="InputGender">Gender <span class="text-danger">*</span></label>
                                </div>
                                @error('Gender')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('location') is-invalid @enderror">
                                    <select class="w-100 form-select @error('location') is-invalid @enderror"
                                        aria-label="Select Location" wire:model="location" id="InputLocation"
                                        required>
                                        <option value="male" selected>Surabaya</option>
                                        <option value="female">Jakarta</option>
                                    </select>
                                    <label for="InputLocation">Location <span class="text-danger">*</span></label>
                                </div>
                                @error('location')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <h6 class="text-muted fw-normal mb-3">Personal Information</h6>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('email') is-invalid @enderror">
                                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                                        id="floatingInputEmail" placeholder="name@example.com"
                                        wire:model.defer="email">
                                    <label for="floatingInputEmail">Email Address <span
                                            class="text-danger">*</span></label>
                                </div>
                                @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('phone') is-invalid @enderror">
                                    <input type="email" class="form-control @error('phone') is-invalid @enderror"
                                        id="floatingInputPhone" placeholder="name@example.com"
                                        wire:model.defer="phone" pattern="[0-9]">
                                    <label for="floatingInputPhone">Phone Number <span
                                            class="text-danger">*</span></label>
                                </div>
                                @error('phone')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <a href="javascript:void(0)" class="fs-6" data-bs-toggle="modal"
                                data-bs-target="#changePasswordModal">Want to change password?</a>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" wire:click="checkEditedEmail">Save
                                changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- End edit profile modal --}}

        {{-- Start change password modal --}}
        <div class="modal fade" wire:ignore.self id="changePasswordModal" tabindex="-2"
            aria-labelledby="changePasswordModal" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered ">
                <div class="modal-content">
                    <form action="">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="editProfileModalLabel">Change Password</h1>
                            <a href="javascript::void(0)" class="h-100 text-decoration-none me-1"
                                data-bs-dismiss="modal" aria-label="Close"><i
                                    class="fa-solid fa-xmark text-dark fs-5"></i></a>
                        </div>
                        <div class="modal-body">
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('old_password') is-invalid @enderror">
                                    <input type="password"
                                        class="form-control @error('old_password') is-invalid @enderror"
                                        id="oldPassword" placeholder="********" wire:model.defer="old_password">
                                    <label for="oldPassword">Old Password <span class="text-danger">*</span></label>
                                </div>
                                @error('old_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('new_password') is-invalid @enderror">
                                    <input type="password"
                                        class="form-control @error('new_password') is-invalid @enderror"
                                        id="newPassword" placeholder="********" wire:model.defer="new_password">
                                    <label for="newPassword">New Password <span class="text-danger">*</span></label>
                                </div>
                                @error('new_password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group has-validation mb-3">
                                <div class="form-floating @error('new_password_confirmation') is-invalid @enderror">
                                    <input type="password"
                                        class="form-control @error('new_password_confirmation') is-invalid @enderror"
                                        id="confirmNewPassword" placeholder="********"
                                        wire:model.defer="new_password_confirmation">
                                    <label for="confirmNewPassword">New Password Confirmation<span
                                            class="text-danger">*</span></label>
                                </div>
                                @error('new_password_confirmation')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" wire:click="changePassword">Save
                                changes</button>
                        </div>
                    </form>
                </div>
            </div>
            {{-- <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content overflow-hidden">
                    <div class="modal-header d-flex justify-content-end border-0">
                        <h1 class="modal-title fs-5" id="editProfileModalLabel">Change Password</h1>
                        <a href="javascript::void(0)" class="h-100 text-decoration-none me-1" data-bs-dismiss="modal"
                            aria-label="Close">
                            <i class="fa-solid fa-xmark text-dark fs-5"></i>
                        </a>
                    </div>
                    <div class="modal-body p-0 d-flex align-items-center justify-content-center">
                        <h6 class="text-muted fw-normal mb-3">Change Password (Require only want to change)</h6>
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('old_password') is-invalid @enderror">
                                <input type="password"
                                    class="form-control @error('old_password') is-invalid @enderror" id="oldPassword"
                                    placeholder="********" wire:model.defer="old_password">
                                <label for="oldPassword">Old Password <span class="text-danger">*</span></label>
                            </div>
                            @error('old_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('new_password') is-invalid @enderror">
                                <input type="password"
                                    class="form-control @error('new_password') is-invalid @enderror" id="newPassword"
                                    placeholder="********" wire:model.defer="new_password">
                                <label for="newPassword">New Password <span class="text-danger">*</span></label>
                            </div>
                            @error('new_password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="input-group has-validation mb-3">
                            <div class="form-floating @error('new_password_confirmation') is-invalid @enderror">
                                <input type="password"
                                    class="form-control @error('new_password_confirmation') is-invalid @enderror"
                                    id="confirmNewPassword" placeholder="********"
                                    wire:model.defer="new_password_confirmation">
                                <label for="confirmNewPassword">New Password Confirmation<span
                                        class="text-danger">*</span></label>
                            </div>
                            @error('new_password_confirmation')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" wire:click="changePassword">Save
                            changes</button>
                    </div>
                </div>
            </div> --}}
        </div>
        {{-- End change password modal --}}
    </div>

    <div class="row justify-content-center g-0 vh-100 w-100 bg-main">
        {{-- Start left side area --}}
        <div class="col-xxl-3 col-lg-4 h-100 d-flex flex-column bg-white" wire:key='left'>
            <div class="d-flex justify-content-evenly bg-light2 sticky-top shadow-sm">
                <div class="d-flex justify-content-center p-3 {{ $side_menu == 1 ? 'side-menu-active' : '' }}"
                    style="">
                    <a href="javascript:void(0)"
                        class="d-flex flex-column align-items-center text-decoration-none position-relative"
                        wire:click="changeMenu(1)">
                        <img src="{{ asset('img/logo.png') }}" alt="Logo Hitch.Ink" class=""
                            style="height: 1.5rem; width: auto;">
                        {{-- <i class="fa-solid fa-comments fs-5 pb-1"></i> --}}
                        <h6 class="mb-0 text-dark">Chats</h6>
                        @if (isset($new_msg_count) && $new_msg_count > 0)
                            <span
                                class="position-absolute top-0 start-100 translate-middle p-1 bg-danger border rounded-circle">
                                <span class="visually-hidden">new msg amount</span>
                            </span>
                        @endif
                    </a>
                </div>
                <div class="d-flex justify-content-center p-3 {{ $side_menu == 2 ? 'side-menu-active' : '' }}"
                    style="">
                    <a href="javascript:void(0)"
                        class="d-flex flex-column align-items-center text-decoration-none text-dark"
                        wire:click="changeMenu(2)">
                        {{-- <i class="fa-solid fa-user-group fs-5 pb-1"></i> --}}
                        <img src="{{ asset('img/icons/After/user-group-solid.svg') }}" height="24" alt="icon"
                            class="pb-1">
                        <h6 class="mb-0">Friends</h6>
                    </a>
                </div>
                <div class="d-flex justify-content-center p-3 {{ $side_menu == 3 ? 'side-menu-active' : '' }}"
                    style="">
                    <a href="javascript:void(0)"
                        class="d-flex flex-column align-items-center text-decoration-none text-dark position-relative"
                        wire:click="changeMenu(3)">
                        {{-- <i class="fa-solid fa-users fs-5 pb-1"></i> --}}
                        <img src="{{ asset('img/icons/After/users-solid.svg') }}" height="24" alt="icon"
                            class="pb-1">
                        <h6 class="mb-0">Requests</h6>
                        @if (isset($new_request_count) && $new_request_count > 0)
                            <span
                                class="position-absolute top-0 start-100 translate-middle p-1 bg-danger border rounded-circle">
                                <span class="visually-hidden">new request amount</span>
                            </span>
                        @endif
                    </a>
                </div>
                <div class="d-flex justify-content-center p-3" style="height: 45px;">
                    <a href="javascript:void(0)"
                        class="d-flex flex-column align-items-center text-decoration-none text-dark" id="darkSwitch"
                        wire:click="darkMode(1)" wire:ignore>
                        @if (!$dark)
                            <img src="{{ asset('img/icons/After/moon-solid.svg') }}" height="24" alt="icon"
                                class="pb-1">
                            <h6 class="mb-0">Dark</h6>
                        @else
                            <img src="./img/icons/After/sun-solid.svg" height="24" alt="icon" class="pb-1">
                            <h6 class="mb-0">Light</h6>
                        @endif
                    </a>
                </div>
                <div class="d-flex justify-content-center p-3 {{ $side_menu == 4 ? 'side-menu-active' : '' }}">
                    <a href="javascript:void(0)"
                        class="d-flex flex-column align-items-center text-decoration-none text-dark"
                        wire:click="changeMenu(4)">
                        {{-- <i class="fa-solid fa-circle-user fs-5 pb-1"></i> --}}
                        <img src="{{ asset('img/icons/After/circle-user-solid.svg') }}" height="24"
                            alt="icon" class="pb-1">
                        <h6 class="mb-0">Profile</h6>
                    </a>
                </div>
            </div>

            <div class="flex-grow-1 shadow-sm custom-scrollbar" data-scrollbar style="overflow-y: auto;">
                @if ($side_menu == 1)
                    {{-- Chats Menu Start --}}
                    <h4 class="mb-0 ps-5 py-3 border-bottom">Chats</h4>
                    <ul class="list-group rounded-0">
                        @forelse ($chat_rooms as $detail_room)
                            @php
                                $interlocuter = $detail_room->participants
                                    ->where('user_id', '!=', Auth::id())
                                    ->first()
                                    ->user()
                                    ->withTrashed()
                                    ->first();
                                // dd($interlocuter);
                                $latest_chat = $detail_room->chats->where('deleted_from_receiver', 0)->last();
                            @endphp
                            <li class="list-group-item {{ (isset($other_user) ? $other_user->id : 999999999999) === $interlocuter->id ? 'room-chat-active' : '' }} h-auto py-2 border-top-0 border-end-0 d-flex align-items-center open-chat"
                                data-bs-target="#activeChatMobile" data-room-id="{{ $detail_room->id }}"
                                data-user-id="{{ $interlocuter->id }}" style="cursor: pointer;">
                                <div class="h-auto w-auto pe-3 my-1">
                                    <div class="overflow-hidden rounded-circle w-auto">
                                        <img src="{{ asset("img/user/$interlocuter->photo") }}" height="50"
                                            width="50" alt="Generic placeholder image"
                                            style="object-fit: cover;" />
                                    </div>
                                </div>
                                <div class="overflow-hidden my-1">
                                    <h6 class="mb-0 text-dark">{{ $interlocuter->name }}</h6>
                                    <p class="fs-6 text-truncate mb-0 text-muted">
                                        {{-- @dd(isset($latest_chat)) --}}
                                        @if (isset($latest_chat))
                                            @php
                                                $deleted_from_source = $latest_chat->sender_id == $signed_user->id ? $latest_chat->deleted_from_sender : $latest_chat->deleted_from_receiver;
                                            @endphp
                                            @if (!$deleted_from_source)
                                                @if ($latest_chat->is_file)
                                                    @if ($latest_chat->mime_type == 'png' || $latest_chat->mime_type == 'jpg')
                                                        <i class="fa-solid fa-image"
                                                            style="font-size: 16px;"></i>&ensp;<i
                                                            class="fs-6 text-muted fw-semibold">Image</i>
                                                    @elseif ($latest_chat->mime_type == 'mp4')
                                                        <i class="fa-solid fa-video"
                                                            style="font-size: 16px;"></i>&ensp;<i
                                                            class="fs-6 text-muted fw-semibold">Video</i>
                                                    @else
                                                        <i class="fa-solid fa-file"
                                                            style="font-size: 16px;"></i>&ensp;<i
                                                            class="fs-6 text-muted fw-semibold">Document</i>
                                                    @endif
                                                @else
                                                    {{ $latest_chat->content }}
                                                @endif
                                            @endif
                                        @endif
                                    </p>
                                </div>
                                <div class="align-self-stretch my-1 flex-fill d-flex flex-column align-items-end">
                                    <small
                                        class="text-nowrap text-muted">{{ isset($latest_chat) ? $latest_chat->created_at->diffForHumans() : '' }}</small>
                                    @if ($detail_room->unseen_msg_count > 0)
                                        <span
                                            class="badge bg-danger mt-1 rounded-circle fw-bold">{{ $detail_room->unseen_msg_count }}</span>
                                    @endif
                                </div>
                            </li>
                        @empty
                            <li class="d-flex align-items-center justify-content-center flex-wrap">
                                <h6 class="my-5 w-100 text-dark text-center">You doesn't have any chat</h6>
                                <a href="javascript:void(0)" class="text-decoration-none text-center"
                                    wire:click="changeMenu()">Start your first chat now?</a>
                            </li>
                        @endforelse
                    </ul>
                    {{-- Chats Menu End --}}
                @elseif ($side_menu == 2)
                    {{-- Friends Menu Start --}}
                    <div class="px-5 py-3 border-bottom" wire:ignore wire:key="friend">
                        <div class="d-flex justify-content-between overflow-hidden menu-title">
                            <h4 class="chat-list-title mb-0">Friends</h4>
                            <a href="javascript:void"
                                class="btn btn-sm btn-outline-primary rounded-circle open-search text-dark"
                                style="height: 33px; width: 33px;">
                                <i class="fa-solid fa-magnifying-glass fs-6"></i>
                            </a>
                        </div>
                        <div class="search-form overflow-hidden"
                            style="width: 0px; height: 0px; transition: width .5s, height .5s;">
                            <div class="input-group overflow-hidden mb-3">
                                <input type="text" class="form-control" placeholder="Search Friend..."
                                    name="search" wire:model="search" />
                                <button class="btn btn-danger close-search" type="button"
                                    onclick="@this.clearSearchUser()" style="color: white;"><i
                                        class="fa-solid fa-xmark fs-6"></i></button>
                            </div>
                            <div class="d-flex justify-content-between align-items-center pb-2">
                                <select class="form-select form-select-sm me-2" wire:model="search_gender">
                                    <option value=""
                                        {{ $search_gender == '' || $search_gender == null ? 'selected' : '' }}>Select
                                        Gender</option>
                                    <option value="male" {{ $search_gender == 'male' ? 'selected' : '' }}>Male
                                    </option>
                                    <option value="female" {{ $search_gender == 'female' ? 'selected' : '' }}>Female
                                    </option>
                                </select>
                                <select class="form-select form-select-sm ms-2" wire:model="search_location">
                                    <option value=""
                                        {{ $search_location == '' || $search_location == null ? 'selected' : '' }}>
                                        Select Location</option>
                                    <option value="surabaya" {{ $search_location == 'surabaya' ? 'selected' : '' }}>
                                        Surabaya</option>
                                    <option value="jakarta" {{ $search_location == 'jakarta' ? 'selected' : '' }}>
                                        Jakarta</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <ul class="list-group">
                        {{-- @dd($friends) --}}
                        @forelse ($friends as $user)
                            <li class="list-group-item h-auto py-2 border-top-0 border-end-0 open-chat"
                                data-user-id="{{ $user->id }}"
                                wire:click="openRoomIfExist({{ $user->id }})">
                                <a href="javascript:void(0)"
                                    class="w-100 text-decoration-none text-dark d-flex align-items-center">
                                    <div class="h-auto w-auto pe-3 my-1">
                                        <div class="overflow-hidden rounded-circle">
                                            <img src="{{ asset("img/user/{$user->photo}") }}" height="50"
                                                width="50" alt="Generic placeholder image"
                                                style="object-fit: cover;" />
                                        </div>
                                    </div>
                                    <div class="overflow-hidden my-1">
                                        <h6 class="mb-0">{{ $user->name }}</h6>
                                        <p class="fs-6 text-truncate mb-0 text-muted">
                                            {{ $user->bio }}
                                        </p>
                                    </div>
                                </a>
                            </li>
                        @empty
                            <li class="d-flex align-items-center justify-content-center flex-wrap">
                                <h6 class="text-dark text-center my-5 w-100">You doesn't have any friend yet</h6>
                                <a href="javascript:void(0)" wire:click="changeMenu(3)"
                                    class="text-decoration-none text-center">Make a new one now?</a>
                            </li>
                        @endforelse
                    </ul>
                    {{-- Friends Menu End --}}
                @elseif ($side_menu == 3)
                    {{-- Others Menu Start --}}
                    <div class="px-5 py-3 border-bottom" wire:ignore wire:key="other">
                        <div class="d-flex justify-content-between overflow-hidden menu-title">
                            <h4 class="chat-list-title mb-0">Requests</h4>
                            <a href="javascript:void"
                                class="btn btn-sm btn-outline-primary rounded-circle open-search"
                                style="height: 33px; width: 33px">
                                <i class="fa-solid fa-magnifying-glass text-dark fs-6"></i>
                            </a>
                        </div>
                        <div class="search-form overflow-hidden"
                            style="width: 0px; height: 0px; transition: width .5s, height .5s;">
                            <div class="input-group overflow-hidden mb-3">
                                <input type="text" class="form-control"
                                    placeholder="Search User by Name or Username..." name="search"
                                    wire:model="search" />
                                <button class="btn btn-danger close-search" type="button"
                                    onclick="@this.clearSearchUser()" style="color: white;"><i
                                        class="fa-solid fa-xmark fs-6"></i></button>
                            </div>
                            <div class="d-flex justify-content-between align-items-center pb-2">
                                <select class="form-select form-select-sm me-2" wire:model="search_gender">
                                    <option value=""
                                        {{ $search_gender == '' || $search_gender == null ? 'selected' : '' }}>Select
                                        Gender</option>
                                    <option value="male" {{ $search_gender == 'male' ? 'selected' : '' }}>Male
                                    </option>
                                    <option value="female" {{ $search_gender == 'female' ? 'selected' : '' }}>Female
                                    </option>
                                </select>
                                <select class="form-select form-select-sm ms-2" wire:model="search_location">
                                    <option value=""
                                        {{ $search_location == '' || $search_location == null ? 'selected' : '' }}>
                                        Select Location</option>
                                    <option value="surabaya" {{ $search_location == 'surabaya' ? 'selected' : '' }}>
                                        Surabaya</option>
                                    <option value="jakarta" {{ $search_location == 'jakarta' ? 'selected' : '' }}>
                                        Jakarta</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <h6 class="ms-4 my-2 text-muted">Friend Request</h6>
                    <ul class="list-group rounded-0 border-top">
                        @forelse ($friend_requests as $user)
                            <li class="list-group-item h-auto py-2 border-top-0 border-end-0 open-chat"
                                data-user-id="{{ $user->id }}"
                                wire:click="openRoomIfExist({{ $user->id }})">
                                <a href="javascript:void(0)"
                                    class="w-100 text-decoration-none text-dark d-flex align-items-center">
                                    <div
                                        class="h-auto w-auto pe-3 my-1 d-flex justify-content-center align-items-center">
                                        <img src="{{ asset("img/user/$user->photo") }}" height="50"
                                            width="50" alt="Generic placeholder image" class="rounded-circle"
                                            style="object-fit:cover;" />
                                        <div class="overflow-hidden ms-3 my-1">
                                            <h6 class="mb-0">{{ $user->name }}</h6>
                                            <p class="fs-6 text-truncate mb-0 text-muted">
                                                {{ $user->bio }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 text-end">
                                        <button class="btn btn-sm rounded-circle btn-outline-success"
                                            style="height: 2rem; width: 2rem;"
                                            wire:click="addFriend({{ $user->id }})"><i
                                                class="fa-solid fa-user-plus"></i></button>
                                    </div>
                                </a>
                            </li>
                        @empty
                            <li class="d-flex align-items-center justify-content-center">
                                <h6 class="text-dark text-center my-4">No Friend Request Found</h6>
                            </li>
                        @endforelse
                    </ul>
                    <h6 class="ms-4 my-2 text-muted">Pending Request</h6>
                    <ul class="list-group rounded-0 border-top">
                        @forelse ($pending_requests as $user)
                            <li class="list-group-item h-auto py-2 border-top-0 border-end-0 open-chat"
                                data-user-id="{{ $user->id }}"
                                wire:click="openRoomIfExist({{ $user->id }})">
                                <a href="javascript:void(0)"
                                    class="w-100 text-decoration-none text-dark d-flex align-items-center">
                                    <div
                                        class="h-auto w-auto pe-3 my-1 d-flex justify-content-center align-items-center">
                                        <img src="{{ asset("img/user/$user->photo") }}" height="50"
                                            width="50" alt="Generic placeholder image" class="rounded-circle"
                                            style="object-fit:cover;" />
                                        <div class="overflow-hidden ms-3 my-1">
                                            <h6 class="mb-0">{{ $user->name }}</h6>
                                            <p class="fs-6 text-truncate mb-0 text-muted">
                                                {{ $user->bio }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 text-end">
                                        <button class="btn btn-sm rounded-circle btn-outline-danger"
                                            style="height: 2rem; width: 2rem;"
                                            wire:click="cancelAddFriend({{ $user->id }})"><i
                                                class="fa-solid fa-user-xmark"></i></button>
                                    </div>
                                </a>
                            </li>
                        @empty
                            <li class="d-flex align-items-center justify-content-center">
                                <h6 class="text-dark text-center my-4">No Pending Request Found</h6>
                            </li>
                        @endforelse
                    </ul>
                    <h6 class="ms-4 my-2 text-muted">All User</h6>
                    <ul class="list-group rounded-0 border-top">
                        @forelse ($others as $user)
                            <li class="list-group-item h-auto py-2 border-top-0 border-end-0 open-chat"
                                data-user-id="{{ $user->id }}"
                                wire:click="openRoomIfExist({{ $user->id }})" wire:ignore.self>
                                <a href="javascript:void(0)"
                                    class="w-100 text-decoration-none text-dark d-flex align-items-center">
                                    <div
                                        class="h-auto w-auto pe-3 my-1 d-flex justify-content-center align-items-center">
                                        <img src="{{ asset("img/user/$user->photo") }}" height="50"
                                            width="50" alt="Generic placeholder image" class="rounded-circle"
                                            style="object-fit:cover;" />
                                        <div class="overflow-hidden ms-3 my-1">
                                            <h6 class="mb-0">{{ $user->name }}</h6>
                                            <p class="fs-6 text-truncate mb-0 text-muted">
                                                {{ $user->bio }}
                                            </p>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 text-end">
                                        <button class="btn btn-sm rounded-circle btn-outline-success"
                                            style="height: 2rem; width: 2rem;"
                                            wire:click="addFriend({{ $user->id }})"><i
                                                class="fa-solid fa-user-plus"></i>
                                        </button>
                                    </div>
                                </a>
                            </li>
                        @empty
                            <li class="d-flex align-items-center justify-content-center">
                                <h6 class="text-dark text-center my-4">No Other User Found</h6>
                            </li>
                        @endforelse
                    </ul>
                    {{-- Others Menu End --}}
                @elseif ($side_menu == 4)
                    {{-- Profile Menu Start --}}
                    <h4 class="chat-list-title ps-5 py-3 border-bottom mb-0">User Profile</h4>
                    <div class="d-flex flex-wrap bg-light2">
                        <header class="d-flex justify-content-center w-100 px-4 py-5 bg-white mb-2">
                            <!-- User Profile image with name -->
                            <div class="header-profile-sidebar d-flex flex-column align-items-center ">
                                <div class="position-relative" style="width: 150px; height: 150px;">
                                    <img src="{{ asset("img/user/$signed_user->photo") }}" alt="user_avatar"
                                        class="preview-img rounded-circle overflow-hidden border border-2 border-dark"
                                        style="object-fit: cover; cursor: pointer; width: 150px; height: 150px;"
                                        data-bs-target="#imageModal" />
                                    <span
                                        class="position-absolute translate-middle d-flex align-items-center justify-content-center rounded-circle bg-danger"
                                        style="cursor: pointer; width: 35px; height: 35px; top: 90%; left: 85%; color: white;"
                                        id="resetPhoto">
                                        <i class="fa-solid fa-trash fs-6"></i>
                                        <span class="visually-hidden">Reset Photo</span>
                                    </span>
                                </div>
                                <h4 class="chat-user-name mt-4 mb-0">{{ $signed_user->name }}
                                    {!! $signed_user->gender == 'male'
                                        ? '<i class="fa-solid fa-mars fs-5 text-blue"></i>'
                                        : '<i class="fa-solid fa-venus fs-5" style="color: #e0276a;"></i>' !!}
                                </h4>
                                @if (isset($signed_user->username))
                                    <p class="fs-6 text-muted mb-0">{{ "@$signed_user->username" }}</p>
                                @else
                                    <p class="fs-6 text-muted">
                                        <a href="" class="text-decoration-none text-muted"
                                            data-bs-toggle="modal" data-bs-target="#editProfileModal">
                                            <img src="{{ asset('img/icons/After/pen-to-square-solid.svg') }}"
                                                height="16" alt="icon" class="me-3">
                                            <i>Add your username</i>
                                        </a>
                                    </p>
                                @endif
                                {{-- <p class="fs-6 text-muted">{!! isset($signed_user->username)
									? "@$signed_user->username"
									: '<a href="" class="text-decoration-none text-muted" data-bs-toggle="modal" data-bs-target="#editProfile"><i class="fa-solid fa-pen-to-square me-3"></i><i>Add your username </i></a>' !!}</p> --}}
                            </div>
                            <!--/ User Profile image with name -->
                        </header>
                        <div class="w-100">
                            <!-- About User -->
                            <div class="mb-2 p-4 bg-white text-wrap">
                                <h6 class="text-muted fw-bold mb-2">About</h6>
                                @if (isset($signed_user->bio) && $signed_user->bio !== '')
                                    <p class="fs-6 text-dark mb-0 text-wrap">{{ "$signed_user->bio" }}</p>
                                @else
                                    <div class="fs-6 text-muted overflow-hidden mb-0" wire:ignore.self>
                                        {{-- <a href="javascript:void(0)"
                                            class="text-decoration-none text-muted open-live-input"
                                            data-bs-toggle="modal" data-bs-target="#editProfileModal">
                                            <img src="{{ asset('img/icons/After/pen-to-square-solid.svg') }}"
                                                height="16" alt="icon" class="me-3">
                                            <i>Add your bio</i>
                                        </a> --}}
                                        <a href="javascript:void(0)"
                                            class="text-decoration-none text-muted open-live-input">
                                            <img src="{{ asset('img/icons/After/pen-to-square-solid.svg') }}"
                                                height="16" alt="icon" class="me-3">
                                            <i>Add your bio</i>
                                        </a>
                                    </div>
                                    <form action=""
                                        class="input-group input-group-sm mb-0 overflow-hidden py-2"
                                        style="width: 0px; height: 0px; transition: width .5s, height .5s;"
                                        wire:ignore.self>
                                        <input type="search"
                                            class="form-control @error('bio') is-invalid @enderror"
                                            placeholder="Input Bio Description" aria-label="Input Bio Description"
                                            wire:model.defer="bio">
                                        <button class="btn btn-outline-success close-live-input" type="button"
                                            wire:click="editProfile"><i class="fa-solid fa-check"></i></button>
                                    </form>
                                    @error('bio')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                    {{-- <p class="fs-6 text-muted overflow-hidden mb-0"
                                        style="width: 0px; height: 0px; transition: width .5s, height .5s;">
                                        <a href="javascript:void(0)" class="text-decoration-none text-muted close-live-input"
                                            data-bs-toggle="modal" data-bs-target="#editProfileModal">
                                            <img src="{{ asset('img/icons/After/pen-to-square-solid.svg') }}"
                                                height="16" alt="icon" class="me-3">
                                            <i>Add your bio</i>
                                        </a>
                                    </p> --}}
                                @endif
                            </div>
                            <!-- About User -->

                            <!-- User's personal information -->
                            <div class="mb-2 p-4 bg-white">
                                <div class="personal-info">
                                    <h6 class="text-muted fw-bold mb-2">Personal Information</h6>
                                    <ul class="list-unstyled mb-0">
                                        <li class="mb-1 d-flex align-items-center">
                                            <div class="col-1">
                                                <img src="{{ asset('img/icons/After/envelope-solid.svg') }}"
                                                    height="16" alt="icon" class="me-3">
                                            </div>
                                            <span class="align-middle">{{ $signed_user->email }}</span>
                                        </li>
                                        <li class="mb-1 d-flex align-items-center">
                                            @if (isset($signed_user->phone) && $signed_user->phone !== '')
                                                <div class="col-1">
                                                    <img src="{{ asset('img/icons/After/phone-solid.svg') }}"
                                                        height="16" alt="icon" class="me-3">
                                                </div>
                                                <span class="align-middle">{{ $signed_user->phone }}</span>
                                            @else
                                                <a href="javascript::void(0)"
                                                    class="text-decoration-none text-muted w-100"
                                                    data-bs-toggle="modal" data-bs-target="#editProfileModal">
                                                    <img src="{{ asset('img/icons/After/pen-to-square-solid.svg') }}"
                                                        height="16" alt="icon" class="me-3">
                                                    <i class="text-muted">Add your phone number</i>
                                                </a>
                                            @endif
                                        </li>
                                        <li class="mb-1 d-flex align-items-center">
                                            <div class="col-1">
                                                <img src="{{ asset('img/icons/After/location-dot-solid.svg') }}"
                                                    height="16" alt="icon" class="me-3">
                                            </div>
                                            <span class="align-middle">{{ ucfirst($signed_user->location) }}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--/ User's personal information -->

                            <!-- User's Setting -->
                            <div class="mb-2 p-4 bg-white">
                                <div class="more-options">
                                    <h6 class="text-muted fw-bold mb-2">Settings</h6>
                                    <ul class="list-unstyled mb-0">
                                        <li class="mb-1">
                                            <a href=""
                                                class="d-flex align-items-center text-decoration-none text-dark"
                                                data-bs-toggle="modal" data-bs-target="#editProfileModal">
                                                <div class="col-1">
                                                    <img src="{{ asset('img/icons/After/user-gear-solid.svg') }}"
                                                        height="16" alt="icon" class="me-3">
                                                </div>
                                                <span class="align-middle">Edit Profile</span>
                                            </a>
                                        </li>
                                        <li class="mb-1">
                                            <a href="javascript::void(0)"
                                                class="d-flex align-items-center text-decoration-none text-dark"
                                                id="darkSwitch2" wire:click="darkMode(1)" wire:ignore>
                                                @if (!$dark)
                                                    <div class="col-1">
                                                        <img src="{{ asset('img/icons/After/moon-solid.svg') }}"
                                                            height="16" alt="icon" class="me-3">
                                                    </div>
                                                    <span class="align-middle">Dark Mode</span>
                                                @else
                                                    <div class="col-1">
                                                        <img src="./img/icons/After/sun-solid.svg" height="16"
                                                            alt="icon" class="me-3">
                                                    </div>
                                                    <span class="align-middle">Light Mode</span>
                                                @endif
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--/ User's Setting -->

                            <!-- User's Actions -->
                            <div class="mb-2 p-4 bg-white">
                                <div class="personal-info">
                                    <h6 class="text-muted fw-bold mb-2">Actions</h6>
                                    <ul class="list-unstyled mb-0">
                                        <li class="mb-1">
                                            <a href="javascript::void(0)" wire:click="logout()"
                                                class="d-flex align-items-center text-decoration-none text-danger">
                                                <i class="col-1 fa-solid fa-right-from-bracket fs-6"></i>
                                                <span class="align-middle">Log out</span>
                                            </a>
                                        </li>
                                        <li class="mb-1">
                                            <a href="javascript:void(0)"
                                                class="d-flex align-items-center text-decoration-none text-danger delete-account-btn">
                                                <i class="col-1 fa-solid fa-ban fs-6"></i>
                                                <span class="align-middle">Delete Account</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--/ User's Actions -->
                        </div>
                    </div>
                    {{-- Profile Menu End --}}
                @endif
            </div>
        </div>
        {{-- End left side area --}}

        {{-- Start middle area --}}
        @if (isset($other_user))
            <div class="col d-none d-lg-block h-100 middle-area" wire:key='mid'>
                <div class="d-flex flex-column h-100 w-100">

                    <!-- Chat Header -->
                    <div class="d-flex sticky-top p-3 bg-light2 shadow-sm border border-top-0 border-bottom-0">
                        <div class="d-flex align-items-center flex-grow-1 open-profile"
                            style="height: 45px; cursor: pointer;">
                            <div class="rounded-circle overflow-hidden me-3">
                                <img src="{{ asset("img/user/$other_user->photo") }}" alt="avatar"
                                    height="40" width="40" style="object-fit: cover;"
                                    class="preview-img" />
                            </div>
                            <h6 class="mb-0">{{ $other_user->name }}</h6>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="dropdown">
                                <button class="btn btn-sm text-dark" type="button" data-bs-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false" wire:ignore.self>
                                    <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="chat-header-actions"
                                    wire:ignore.self>
                                    <a class="dropdown-item open-profile" href="javascript:void(0)">View
                                        Profile</a>
                                    <a class="dropdown-item delete-all-chats" href="javascript:void(0)">Clear
                                        All
                                        Chat</a>
                                    @if ($is_friend === true)
                                        <a class="dropdown-item unfriend-btn" data-id="{{ $other_user->id }}"
                                            data-name="{{ $other_user->name }}"
                                            href="javascript:void(0)">Unfriend</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Chat Header -->

                    <!-- Active Chat -->
                    <div class="active-chat p-3 flex-grow-1 custom-scrollbar" style="overflow-y: auto"
                        wire:key="chat-{{ $other_user->id }}" wire:poll>
                        <!-- User Chat messages -->
                        <div class="user-chats">
                            @forelse ($chats as $chat)
                                @php
                                    if ($chat->deleted_for_receiver) {
                                        $chat->content = 'Pesan ini telah dihapus!';
                                    }
                                @endphp
                                @if ($chat->sender_id == $signed_user->id)
                                    @if ($chat->deleted_from_sender == 1)
                                        @continue
                                    @endif
                                    @if ($chat->is_file && $chat->deleted_for_receiver != 1)
                                        @if ($chat->mime_type == 'png' || $chat->mime_type == 'jpg')
                                            <div class="chat d-flex flex-wrap flex-row-reverse">
                                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                                    <div
                                                        class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                        <div class="px-3 pt-3 position-relative mb-1"
                                                            style="max-width: 30rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #4c49fd 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                            <div class="chat-content text-wrap d-flex justify-content-end"
                                                                style="min-width: 15px">
                                                                <img src="{{ asset("img/chat/$chat->content") }}"
                                                                    alt="content" class="w-100 h-100 preview-img"
                                                                    style="object-fit: cover; cursor: pointer; max-height: 500px;"
                                                                    data-bs-target="#imageModal">
                                                            </div>
                                                            @if (!$chat->deleted_for_receiver)
                                                                @if (!$chat->is_seen)
                                                                    <div class="position-absolute bottom-0 start-0">
                                                                        <i class="fa-solid fa-check text-light"
                                                                            style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                    </div>
                                                                @else
                                                                    <div class="position-absolute bottom-0 start-0">
                                                                        <i class="fa-solid fa-check-double text-light"
                                                                            style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                    </div>
                                                                @endif
                                                                <div class="position-absolute bottom-0 end-0">
                                                                    <p class="text-light mb-0"
                                                                        style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                        {{ $chat->created_at->format('H:i') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="dropdown me-1 mt-1">
                                                            <a class="text-decoration-none text-muted px-2"
                                                                href="javascript:void(0)" role="button"
                                                                data-bs-toggle="dropdown" aria-expanded="false"
                                                                wire:ignore.self>
                                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                            </a>
                                                            <ul class="dropdown-menu" wire:ignore.self>
                                                                <li class="dropdown-item text-danger unsend-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i> Unsend
                                                                    Message
                                                                </li>
                                                                <li class="dropdown-item text-danger delete-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                    Delete Message
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif ($chat->mime_type == 'mp4')
                                            <div class="chat d-flex flex-wrap flex-row-reverse">
                                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                                    <div
                                                        class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                        <div class="px-3 pt-3 position-relative mb-1"
                                                            style="max-width: 30rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #3399ff 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                            <div class="chat-content text-wrap position-relative"
                                                                style="min-width: 15px">
                                                                <i class="fa-solid fa-video position-absolute ms-2 mt-2 fs-5"
                                                                    style="color: white;"></i>
                                                                <video class="w-100 h-100 preview-vid"
                                                                    style="object-fit: cover; cursor: pointer; max-height: 500px;"
                                                                    data-bs-target="#videoModal" nocontrols>
                                                                    <source
                                                                        src="{{ asset("vid/chat/$chat->content") }}">
                                                                    Your browser does not support HTML video.
                                                                </video>
                                                            </div>
                                                            @if (!$chat->deleted_for_receiver)
                                                                <div class="position-absolute bottom-0 end-0">
                                                                    <p class="text-light mb-0"
                                                                        style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                        {{ $chat->created_at->format('H:i') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="dropdown me-1 mt-1">
                                                            <a class="text-decoration-none text-muted px-2"
                                                                href="javascript:void(0)" role="button"
                                                                data-bs-toggle="dropdown" aria-expanded="false"
                                                                wire:ignore.self>
                                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                            </a>
                                                            <ul class="dropdown-menu" wire:ignore.self>
                                                                <li class="dropdown-item text-danger unsend-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i> Unsend
                                                                    Message
                                                                </li>
                                                                <li class="dropdown-item text-danger delete-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                    Delete Message
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="chat d-flex flex-wrap flex-row-reverse">
                                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                                    <div
                                                        class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                        <div class="px-3 pt-3 position-relative mb-1"
                                                            style="max-width: 30rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #3399ff 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                            <div class="chat-content text-wrap d-flex justify-content-end"
                                                                style="min-width: 15px">
                                                                <a href="{{ asset("doc/chat/$chat->content") }}"
                                                                    download="{{ $chat->content }}"
                                                                    class="text-decoration-none text-dark mw-100"
                                                                    data-bs-toggle="tooltip"
                                                                    data-bs-title="{{ $chat->content }}">
                                                                    <div class="bg-secondary rounded-1 py-2 px-3 d-flex align-items-center"
                                                                        style="--bs-bg-opacity: .5;">
                                                                        <i
                                                                            class="fa-solid fa-file-arrow-down fs-5"></i>
                                                                        <div class="ps-3 mw-100">
                                                                            <h6
                                                                                class="mb-0 text-normal text-truncate pe-2">
                                                                                {{ $chat->content }}
                                                                            </h6>
                                                                            <p class="mb-0"
                                                                                style="font-size: .7rem;">
                                                                                {{ $chat->file_size >= 1000000 ? round($chat->file_size / 1000000) . ' MB' : ($chat->file_size >= 1000 ? round($chat->file_size / 1000) . ' KB' : $chat->file_size . ' Byte') }}
                                                                                &nbsp;|&nbsp;
                                                                                {{ strtoupper($chat->mime_type) }}</p>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            @if (!$chat->deleted_for_receiver)
                                                                <div class="position-absolute bottom-0 end-0">
                                                                    <p class="text-light mb-0"
                                                                        style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                        {{ $chat->created_at->format('H:i') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="dropdown me-1 mt-1">
                                                            <a class="text-decoration-none text-muted px-2"
                                                                href="javascript:void(0)" role="button"
                                                                data-bs-toggle="dropdown" aria-expanded="false"
                                                                wire:ignore.self>
                                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                            </a>
                                                            <ul class="dropdown-menu" wire:ignore.self>
                                                                <li class="dropdown-item text-danger unsend-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i> Unsend
                                                                    Message
                                                                </li>
                                                                <li class="dropdown-item text-danger delete-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                    Delete Message
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @else
                                        <div class="chat d-flex flex-wrap flex-row-reverse">
                                            <div class="chat-section mb-3" style="max-width: 40rem;">
                                                <div class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                                    <div class="px-3 pt-2 position-relative mb-1"
                                                        style="max-width: 30rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #4c49fd 120%); padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                        <div class="chat-content text-wrap d-flex justify-content-end"
                                                            style="min-width: 25px">
                                                            <p class="fs-6 mb-0 lh-sm" style="color: white;">
                                                                {{ $chat->content }}</p>
                                                        </div>
                                                        @if (!$chat->deleted_for_receiver)
                                                            @if (!$chat->is_seen)
                                                                <div class="position-absolute bottom-0 start-0">
                                                                    <i class="fa-solid fa-check text-light"
                                                                        style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                </div>
                                                            @else
                                                                <div class="position-absolute bottom-0 start-0">
                                                                    <i class="fa-solid fa-check-double text-light"
                                                                        style="font-size: .65rem; padding-bottom: .16rem; padding-left: .5rem;"></i>
                                                                </div>
                                                            @endif
                                                            <div class="position-absolute bottom-0 end-0">
                                                                <p class="text-light mb-0"
                                                                    style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                    {{ $chat->created_at->format('H:i') }}</p>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="dropdown me-1 mt-1">
                                                        <a class="text-decoration-none text-muted px-2"
                                                            href="javascript:void(0)" role="button"
                                                            data-bs-toggle="dropdown" aria-expanded="false"
                                                            wire:ignore.self>
                                                            <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                        </a>
                                                        <ul class="dropdown-menu" wire:ignore.self>
                                                            <li class="dropdown-item text-danger unsend-msg"
                                                                style="cursor: pointer;"
                                                                data-id="{{ $chat->id }}">
                                                                <i class="fa-solid fa-trash fs-6 me-2"></i> Unsend
                                                                Message
                                                            </li>
                                                            <li class="dropdown-item text-danger delete-msg"
                                                                style="cursor: pointer;"
                                                                data-id="{{ $chat->id }}">
                                                                <i class="fa-solid fa-trash fs-6 me-2"></i>
                                                                Delete Message
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    @if ($chat->deleted_from_receiver == 1)
                                        @continue
                                    @endif
                                    @if ($chat->is_file && $chat->deleted_for_receiver != 1)
                                        @if ($chat->mime_type == 'png' || $chat->mime_type == 'jpg')
                                            <div class="chat d-flex align-items-stretch">
                                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                                    <div class="chat-bubble d-flex align-items-strecth">
                                                        <div class="px-3 pt-3 mb-1 position-relative border bg-white"
                                                            style="max-width: 30rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                            <div class="chat-content text-wrap"
                                                                style="min-width: 15px">
                                                                <img src="{{ asset("img/chat/$chat->content") }}"
                                                                    alt="content" class="w-100 h-100 preview-img"
                                                                    style="object-fit: cover; cursor: pointer;"
                                                                    data-bs-target="#imageModal">
                                                            </div>
                                                            @if (!$chat->deleted_for_receiver)
                                                                <div class="position-absolute bottom-0 end-0">
                                                                    <p class="text-muted mb-0"
                                                                        style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                        {{ $chat->created_at->format('H:i') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="dropdown ms-1 mt-1">
                                                            <a class="text-decoration-none text-muted px-2"
                                                                href="javascript:void(0)" role="button"
                                                                data-bs-toggle="dropdown" aria-expanded="false"
                                                                wire:ignore.self>
                                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                            </a>
                                                            <ul class="dropdown-menu" wire:ignore.self>
                                                                <li class="dropdown-item text-danger delete-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i> Delete
                                                                    Message
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif ($chat->mime_type == 'mp4')
                                            <div class="chat d-flex align-items-stretch">
                                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                                    <div class="chat-bubble d-flex align-items-strecth">
                                                        <div class="px-3 pt-3 mb-1 position-relative border bg-white"
                                                            style="max-width: 30rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                            <div class="chat-content text-wrap position-relative"
                                                                style="min-width: 15px">
                                                                <i class="fa-solid fa-video position-absolute ms-2 mt-2 fs-5"
                                                                    style="color: white;"></i>
                                                                <video class="w-100 h-100 preview-vid"
                                                                    style="object-fit: cover; cursor: pointer; max-height: 500px;"
                                                                    data-bs-target="#videoModal" nocontrols>
                                                                    <source
                                                                        src="{{ asset("vid/chat/$chat->content") }}">
                                                                    Your browser does not support HTML video.
                                                                </video>
                                                            </div>
                                                            @if (!$chat->deleted_for_receiver)
                                                                <div class="position-absolute bottom-0 end-0">
                                                                    <p class="text-muted mb-0"
                                                                        style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                        {{ $chat->created_at->format('H:i') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="dropdown ms-1 mt-1">
                                                            <a class="text-decoration-none text-muted px-2"
                                                                href="javascript:void(0)" role="button"
                                                                data-bs-toggle="dropdown" aria-expanded="false"
                                                                wire:ignore.self>
                                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                            </a>
                                                            <ul class="dropdown-menu" wire:ignore.self>
                                                                <li class="dropdown-item text-danger delete-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i> Delete
                                                                    Message
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <div class="chat d-flex align-items-stretch">
                                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                                    <div class="chat-bubble d-flex align-items-strecth">
                                                        <div class="px-3 pt-3 mb-1 position-relative border bg-white"
                                                            style="max-width: 30rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                            <div class="chat-content text-wrap"
                                                                style="min-width: 15px">
                                                                <a href="{{ asset("doc/chat/$chat->content") }}"
                                                                    download="{{ $chat->content }}"
                                                                    class="text-decoration-none text-dark mw-100"
                                                                    data-bs-toggle="tooltip"
                                                                    data-bs-title="{{ $chat->content }}">
                                                                    <div class="bg-secondary rounded-1 py-2 px-3 d-flex align-items-center"
                                                                        style="--bs-bg-opacity: .5;">
                                                                        <i
                                                                            class="fa-solid fa-file-arrow-down fs-5"></i>
                                                                        <div class="ps-3 mw-100">
                                                                            <h6
                                                                                class="mb-0 text-normal text-truncate pe-2">
                                                                                {{ $chat->content }}
                                                                            </h6>
                                                                            <p class="mb-0"
                                                                                style="font-size: .7rem;">
                                                                                {{ $chat->file_size >= 1000000 ? round($chat->file_size / 1000000) . ' MB' : ($chat->file_size >= 1000 ? round($chat->file_size / 1000) . ' KB' : $chat->file_size . ' Byte') }}
                                                                                &nbsp;|&nbsp;
                                                                                {{ strtoupper($chat->mime_type) }}</p>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                            @if (!$chat->deleted_for_receiver)
                                                                <div class="position-absolute bottom-0 end-0">
                                                                    <p class="text-muted mb-0"
                                                                        style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                        {{ $chat->created_at->format('H:i') }}</p>
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="dropdown ms-1 mt-1">
                                                            <a class="text-decoration-none text-muted px-2"
                                                                href="javascript:void(0)" role="button"
                                                                data-bs-toggle="dropdown" aria-expanded="false"
                                                                wire:ignore.self>
                                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                            </a>
                                                            <ul class="dropdown-menu" wire:ignore.self>
                                                                <li class="dropdown-item text-danger delete-msg"
                                                                    style="cursor: pointer;"
                                                                    data-id="{{ $chat->id }}">
                                                                    <i class="fa-solid fa-trash fs-6 me-2"></i> Delete
                                                                    Message
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @else
                                        <div class="chat d-flex align-items-stretch">
                                            <div class="chat-section mb-3" style="max-width: 40rem;">
                                                <div class="chat-bubble d-flex align-items-strecth">
                                                    <div class="px-3 pt-2 mb-1 position-relative border bg-white"
                                                        style="max-width: 30rem; width: auto; border-radius: .7rem; padding-bottom: {{ !$chat->deleted_for_receiver ? '1.25rem' : '.75rem' }};">
                                                        <div class="chat-content text-wrap" style="min-width: 25px">
                                                            <p class="fs-6 mb-0 lh-sm">{{ $chat->content }}</p>
                                                        </div>
                                                        @if (!$chat->deleted_for_receiver)
                                                            <div class="position-absolute bottom-0 end-0">
                                                                <p class="text-muted mb-0"
                                                                    style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                                    {{ $chat->created_at->format('H:i') }}</p>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="dropdown ms-1 mt-1">
                                                        <a class="text-decoration-none text-muted px-2"
                                                            href="javascript:void(0)" role="button"
                                                            data-bs-toggle="dropdown" aria-expanded="false"
                                                            wire:ignore.self>
                                                            <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                                        </a>
                                                        <ul class="dropdown-menu" wire:ignore.self>
                                                            <li class="dropdown-item text-danger delete-msg"
                                                                style="cursor: pointer;"
                                                                data-id="{{ $chat->id }}">
                                                                <i class="fa-solid fa-trash fs-6 me-2"></i> Delete
                                                                Message
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            @empty
                                <div class="text-center my-4 text-secondary">
                                    You don't have a chat history with this person.
                                </div>
                            @endforelse
                            {{-- <div class="chat d-flex align-items-stretch">
                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                    <div class="chat-bubble d-flex align-items-strecth">
                                        <div class="px-3 pt-2 mb-1 position-relative border bg-white"
                                            style="max-width: 30rem; width: auto; border-radius: .7rem; padding-bottom: 1.25rem;">
                                            <div class="chat-content text-wrap d-flex align-items-center pt-2">
                                                <a href="" data-bs-toggle="modal" data-bs-target="#imageModal"
                                                    data-bs-image="{{ asset('img/test/test1.jpg') }}">
                                                    <img src="{{ asset('img/test/test1.jpg') }}" alt=""
                                                        class="w-100 h-100" style="object-fit: cover">
                                                </a>
                                            </div>
                                            <div class="position-absolute bottom-0 end-0">
                                                <p class="text-muted mb-0"
                                                    style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                    23.34</p>
                                            </div>
                                        </div>
                                        <div class="dropdown ms-1 mt-1">
                                            <a class="text-decoration-none text-muted px-2" href="javascript:void(0)"
                                                role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item text-danger" href="javascript:void(0)"><i
                                                            class="fa-solid fa-trash fs-6 me-3"></i>Delete Message</a>
                                                </li>
                                                <li><a class="dropdown-item" href="javascript:void(0)">Something else
                                                        here</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            {{-- <div class="chat d-flex flex-wrap flex-row-reverse">
                                <div class="rounded-circle overflow-hidden ms-2 mt-1"
                                    style="height: 2.5rem; width: 2.5rem;">
                                    <img src="{{ asset('img/vuexy/avatar/avatar-s-7.jpg') }}" alt="avatar"
                                        class="w-100 h-100" />
                                </div>
                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                    <div class="chat-bubble d-flex flex-row-reverse align-items-stretch">
                                        <div class="px-3 pt-2 position-relative mb-1"
                                            style="max-width: 30rem; width: auto; border-radius: .7rem; background: linear-gradient(to right, #4846ff -10%, #3399ff 120%); padding-bottom: 1.25rem;">
                                            <div class="chat-content text-wrap d-flex align-items-center pt-2">
                                                <a href="" data-bs-toggle="modal" data-bs-target="#videoModal"
                                                    data-bs-video="{{ asset('vid/vid1.mp4') }}">
                                                    <video class="w-100 h-100" style="object-fit: cover" nocontrols>
                                                        <source src="{{ asset('vid/vid1.mp4') }}">
                                                        Your browser does not support HTML video.
                                                    </video>
                                                </a>
                                            </div>
                                            <div class="position-absolute bottom-0 end-0">
                                                <p class="text-light mb-0"
                                                    style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                    23.34</p>
                                            </div>
                                        </div>
                                        <div class="dropdown me-1 mt-1">
                                            <a class="text-decoration-none text-muted px-2" href="javascript:void(0)"
                                                role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item text-danger" href="javascript:void(0)"><i
                                                            class="fa-solid fa-trash fs-6 me-3"></i>Delete Message</a>
                                                </li>
                                                <li><a class="dropdown-item" href="javascript:void(0)">Something else here</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                            {{-- <div class="chat d-flex align-items-stretch">
                                <div class="rounded-circle overflow-hidden me-2 mt-1"
                                    style="height: 2.5rem; width: 2.5rem;">
                                    <img src="{{ asset('img/vuexy/avatar/avatar-s-11.jpg') }}" alt="avatar"
                                        class="w-100 h-100" />
                                </div>
                                <div class="chat-section mb-3" style="max-width: 40rem;">
                                    <div class="chat-bubble d-flex align-items-strecth">
                                        <div class="px-3 pt-2 mb-1 position-relative border bg-white"
                                            style="max-width: 30rem; width: auto; border-radius: .7rem; padding-bottom: 1.25rem;">
                                            <div class="chat-content text-wrap d-flex align-items-center pt-2">
                                                <a href="{{ asset('img/1x1 padding.png') }}"
                                                    download="proposed_file_name"
                                                    class="text-decoration-none text-dark mw-100"
                                                    data-bs-toggle="tooltip" data-bs-title="Coba aja ni.txt">
                                                    <div class="bg-secondary rounded-1 py-2 px-3 d-flex align-items-center"
                                                        style="--bs-bg-opacity: .5;">
                                                        <i class="fa-solid fa-file-arrow-down fs-5"></i>
                                                        <div class="ps-3 mw-100">
                                                            <h6 class="mb-0 text-normal text-truncate pe-2">
                                                                Coba aja ni.txt</h6>
                                                            <p class="mb-0" style="font-size: .7rem;">26 kB
                                                                &nbsp;|&nbsp; TXT
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="position-absolute bottom-0 end-0">
                                                <p class="text-muted mb-0"
                                                    style="font-size: .7rem; padding-bottom: .14rem; padding-right: .5rem;">
                                                    23.34</p>
                                            </div>
                                        </div>
                                        <div class="dropdown ms-1 mt-1">
                                            <a class="text-decoration-none text-muted px-2" href="javascript:void(0)"
                                                role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <i class="fa-solid fa-ellipsis-vertical fs-6"></i>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item text-danger" href="javascript:void(0)"><i
                                                            class="fa-solid fa-trash fs-6 me-3"></i>Delete Message</a>
                                                </li>
                                                <li><a class="dropdown-item" href="javascript:void(0)">Something else here</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                        <!-- User Chat messages -->
                    </div>
                    <!--/ Active Chat -->

                    <!-- Submit Chat form -->
                    <div class="chat-app-form sticky-bottom px-3 py-3 bg-light2 shadow-sm">
                        @if ($is_friend == true)
                            @if (is_null($other_user->deleted_at))
                                @if (!$is_friend_blocked)
                                    <form action="" method="post" class="input-group" wire:submit.prevent>
                                        <input type="text" class="form-control border-end-0"
                                            wire:model="chat_input" id=""
                                            placeholder="Type your messages here!..." style="resize: none;">
                                        <span class="input-group-text border-start-0 bg-white">
                                            <a href="javascript:void(0)" onclick="this.nextElementSibling.click()">
                                                <i class="fa-solid fa-file-arrow-down fs-6"></i>
                                            </a>
                                            <input class="form-control d-none" type="file" id="mediaChat"
                                                wire:model="media_chat">
                                        </span>
                                        <button class="btn btn-outline-primary" type="submit" id="button-addon2"
                                            wire:click.prevent="submitChat()">Send</button>
                                    </form>
                                @else
                                    <div class="text-center">
                                        <p class="my-2 text-dark">You're blocked this person, unblock first to start
                                            chatting!</p>
                                    </div>
                                @endif
                            @else
                                <div class="text-center">
                                    <p class="my-2 text-dark">Sorry, This account was no longer exist. You can't send
                                        message to this person again!</p>
                                </div>
                            @endif
                        @else
                            <div class="text-center">
                                <p class="my-2 text-dark">You must be friend first to exchange
                                    messages with this person.</p>
                            </div>
                        @endif
                    </div>
                    <!--/ Submit Chat form -->

                </div>
            </div>
            <script>
                $(document).ready(function() {
                    setInterval(() => {
                        @this.refreshChat();
                    }, 1000);
                });
            </script>
        @else
            <!-- To load Conversation -->
            <div class="col d-none d-lg-flex h-100 align-items-center justify-content-center middle-area close-left"
                wire:key='mid'>
                <div class="start-chat-area d-flex align-items-center justify-content-center flex-wrap">
                    <div class="w-100">
                        <div class="d-flex justify-content-center align-items-center">
                            <div class="rounded-circle btn-neumorph-light p-5 " wire:click="changeMenu()"
                                style="cursor: pointer;">
                                <img src="{{ asset('img/logo.png') }}" alt="Logo Hitch.Ink"
                                    style="height: 13rem; width: auto;">
                            </div>
                        </div>
                    </div>
                    <h1 class="text-center my-5 start-chat-text w-100" style="cursor: default;"><span
                            class="text-green">Start
                            Conversation</span> <span class="text-blue">Now</span></h1>
                    <div class="btn btn-lg btn-outline-primary rounded-5 px-5 py-3 fw-semibold"
                        wire:click="changeMenu()">
                        Click here
                    </div>
                </div>
            </div>
            <!--/ To load Conversation -->
        @endif
        {{-- End middle area --}}

        {{-- Start right side area --}}
        @if (isset($other_user))
            @if ($is_friend == false)
                <!-- Other profile right area -->
                <div class="col-xxl-3 d-none d-xxl-block h-100 bg-light2 left-area" wire:ignore.self
                    wire:key='right'>
                    <div class="d-flex flex-column h-100 w-100">
                        {{-- Start Others Profile photo, name, and username user --}}
                        <div class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                            <div class="d-flex align-items-center justify-content-between w-100"
                                style="height: 45px;">
                                <h4 class="chat-list-title mb-0">Other Profile</h4>
                                <a href="javascript:void(0)"
                                    class="text-decoration-none text-dark btn btn-sm close-profile">
                                    <i class="fa-solid fa-xmark fs-5 text-dark"></i>
                                </a>
                            </div>
                        </div>
                        {{-- End Others Profile photo, name, and username user --}}

                        {{-- Start Others Profile Bio, Phone, Email, etc --}}
                        <div class="w-100" data-scrollbar>
                            <!-- User Profile image with name -->
                            <header class="d-flex justify-content-center w-100 p-4 bg-white">
                                <div class="header-profile-sidebar d-flex flex-column align-items-center ">
                                    <div class="d-flex justify-content-center align-items-center rounded-circle border border-2 border-dark overflow-hidden"
                                        style="width: 150px; height: 150px;">
                                        <img src="{{ asset("img/user/$other_user->photo") }}" alt="user_avatar"
                                            class="h-100 w-100 preview-img"
                                            style="object-fit: cover; cursor: pointer;"
                                            data-bs-target="#imageModal" />
                                    </div>
                                    <h4 class="chat-user-name mt-4 mb-0">{{ $other_user->name }}
                                        {!! is_null($other_user->deleted_at)
                                            ? ($other_user->gender == 'male'
                                                ? '<i class="fa-solid fa-mars fs-5 text-blue"></i>'
                                                : '<i class="fa-solid fa-venus fs-5" style="color: #e0276a;"></i>')
                                            : '' !!}
                                    </h4>
                                    @if (isset($other_user->username))
                                        <p class="fs-6 text-muted mb-0">
                                            {{ is_null($other_user->deleted_at) ? "@$other_user->username" : 'This Account was deleted' }}
                                        </p>
                                    @else
                                        <p class="fs-6 text-muted mb-0">
                                            <i>{{ is_null($other_user->deleted_at) ? "This user doesn't have username" : 'This Account was deleted' }}</i>
                                        </p>
                                    @endif
                                </div>
                            </header>
                            <!--/ User Profile image with name -->

                            <!-- User Profile information and action -->
                            @if (is_null($other_user->deleted_at))

                                <div class="w-100">
                                    <div class="my-2 p-4 bg-white">
                                        <!-- About User -->
                                        <h6 class="text-muted fw-bold mb-2">About</h6>
                                        @if (isset($other_user->bio))
                                            <p class="fs-6 text-dark mb-0">{{ "$other_user->bio" }}</p>
                                        @else
                                            <p class="fs-6 text-muted mb-0"><i>This user doesn't have bio</i></p>
                                        @endif
                                        <!-- About User -->
                                    </div>

                                    <div class="mt-2 p-4 bg-white">
                                        <!-- User's Links -->
                                        <div class="more-options">
                                            <h6 class="text-muted fw-bold mb-2">Actions</h6>
                                            <ul class="list-unstyled mb-0">
                                                @if ($is_friend_request)
                                                    <li class="mb-1">
                                                        <a href="javascript:void(0)"
                                                            class="d-flex align-items-center text-decoration-none text-success"
                                                            wire:click="addFriend({{ $other_user->id }})">
                                                            <i class="col-1 fa-solid fa-user-plus fs-6"></i>
                                                            <span class="align-middle">Accept Friend Request</span>
                                                        </a>
                                                    </li>
                                                @elseif ($is_request_friend)
                                                    <li class="mb-1">
                                                        <a href="javascript:void(0)"
                                                            class="d-flex align-items-center text-decoration-none text-danger"
                                                            wire:click="cancelAddFriend({{ $other_user->id }})">
                                                            <i class="col-1 fa-solid fa-user-xmark fs-6"></i>
                                                            <span class="align-middle">Cancel Request Friend</span>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li class="mb-1">
                                                        <a href="javascript:void(0)"
                                                            class="d-flex align-items-center text-decoration-none text-success"
                                                            wire:click="addFriend({{ $other_user->id }})">
                                                            <i class="col-1 fa-solid fa-user-plus fs-6"></i>
                                                            <span class="align-middle">Add User as Friend</span>
                                                        </a>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                        <!--/ User's Links -->
                                    </div>

                                </div>
                            @else
                                <div class="my-2 p-4 bg-white text-center">
                                    <h6 class="text-muted fw-bold mb-2">Sorry, this Account was no longer exist.</h6>
                                    <a href="javascript:void(0)"
                                        class="d-flex align-items-center justify-content-center text-decoration-none text-danger unfriend-btn"
                                        data-id="{{ $other_user->id }}" data-name="{{ $other_user->name }}"
                                        data-type="leave-chat-room">
                                        <i class="col-1 fa-solid fa-user-minus fs-6"></i>
                                        <span class="align-middle">Leave and Delete this chat room.</span>
                                    </a>
                                </div>
                            @endif
                            <!--/ User Profile information and action -->
                        </div>
                        {{-- Edit Others Profile Bio, Phone, Email, etc --}}
                    </div>
                </div>
                <!--/ Other profile right area -->
            @else
                <!-- Friend profile right area -->
                <div class="col-xxl-3 d-none d-xxl-block h-100 bg-light2 left-area" wire:ignore.self
                    wire:key='right'>

                    <!-- Friend profile right area -->
                    <div class="d-flex flex-column h-100 w-100 detail-profile" wire:ignore.self>
                        <div class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                            <div class="d-flex align-items-center justify-content-between w-100"
                                style="height: 45px;">
                                <h4 class="chat-list-title mb-0">Friend Profile</h4>
                                <a href="javascript:void(0)" class="text-decoration-none btn btn-sm close-profile">
                                    <i class="fa-solid fa-xmark fs-5 text-dark"></i>
                                </a>
                            </div>
                        </div>
                        <div class="w-100 custom-scrollbar" style="overflow-y: auto" data-scrollbar>
                            <header class="d-flex justify-content-center w-100 p-4 bg-white">
                                <!-- User Profile image with name -->
                                <div class="header-profile-sidebar d-flex flex-column align-items-center ">
                                    <div class="d-flex justify-content-center align-items-center rounded-circle border border-2 border-dark overflow-hidden"
                                        style="width: 150px; height: 150px;">
                                        <img src="{{ asset("img/user/$other_user->photo") }}" alt="user_avatar"
                                            class="h-100 w-100 preview-img" data-bs-target="#imageModal"
                                            style="object-fit: cover; cursor: pointer;" />
                                    </div>
                                    <h4 class="chat-user-name mt-4 mb-0">
                                        {{ $other_user->name }}
                                        {!! is_null($other_user->deleted_at)
                                            ? ($other_user->gender == 'male'
                                                ? '<i class="fa-solid fa-mars fs-5 text-blue"></i>'
                                                : '<i class="fa-solid fa-venus fs-5" style="color: #e0276a;"></i>')
                                            : '' !!}
                                    </h4>
                                    @if (isset($other_user->username))
                                        <p class="fs-6 text-muted mb-0">
                                            {{ is_null($other_user->deleted_at) ? "@$other_user->username" : 'This Account was deleted' }}
                                        </p>
                                    @else
                                        <p class="fs-6 text-muted mb-0">
                                            <i>{{ is_null($other_user->deleted_at) ? "This user doesn't have username" : 'This Account was deleted' }}</i>
                                        </p>
                                    @endif
                                </div>
                                <!--/ User Profile image with name -->
                            </header>
                            <div class="w-100">
                                @if (is_null($other_user->deleted_at))
                                    <div class="my-2 p-4 bg-white">
                                        <!-- About User -->
                                        <h6 class="text-muted fw-bold mb-2">About</h6>
                                        @if (isset($other_user->bio))
                                            <p class="fs-6 text-dark mb-0">{{ "$other_user->bio" }}</p>
                                        @else
                                            <p class="fs-6 text-muted mb-0"><i>This user doesn't have bio</i></p>
                                        @endif
                                        <!-- About User -->
                                    </div>

                                    <div class="my-2 p-4 bg-white">
                                        <!-- User's personal information -->
                                        <div class="personal-info">
                                            <h6 class="text-muted fw-bold mb-2">Personal Information</h6>
                                            <ul class="list-unstyled mb-0">
                                                <li class="mb-1 d-flex align-items-center">
                                                    <div class="col-1">
                                                        <img src="{{ asset('img/icons/After/envelope-solid.svg') }}"
                                                            height="16" alt="icon" class="me-3">
                                                    </div>
                                                    <span class="align-middle">{{ $other_user->email }}</span>
                                                </li>
                                                <li class="mb-1 d-flex align-items-center">
                                                    <div class="col-1">
                                                        <img src="{{ asset('img/icons/After/phone-solid.svg') }}"
                                                            height="16" alt="icon" class="me-3">
                                                    </div>
                                                    <span class="align-middle">{!! $other_user->phone
                                                        ? $other_user->phone
                                                        : '<i class="fs-6 text-muted">This user doesn`t have phone number</i>' !!}</span>
                                                </li>
                                                <li class="mb-1 d-flex align-items-center">
                                                    <div class="col-1">
                                                        <img src="{{ asset('img/icons/After/location-dot-solid.svg') }}"
                                                            height="16" alt="icon" class="me-3">
                                                    </div>
                                                    <span
                                                        class="align-middle">{{ ucfirst($other_user->location) }}</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--/ User's personal information -->
                                    </div>

                                    <div class="my-2 p-4 bg-white">
                                        <!-- User's Links -->
                                        <div class="more-options">
                                            <h6 class="text-muted fw-bold mb-2">Options</h6>
                                            <ul class="list-unstyled mb-0">
                                                <li class="mb-1">
                                                    <a href="javascript:void(0)"
                                                        class="row align-items-center text-decoration-none text-dark gx-0 open-profile-media"
                                                        data-menu="document">
                                                        <div class="col-1">
                                                            <img src="{{ asset('img/icons/After/file-solid.svg') }}"
                                                                height="16" alt="icon">
                                                        </div>
                                                        <span class="align-middle col-11">Document Files</span>
                                                    </a>
                                                </li>
                                                <li class="mb-1">
                                                    <a href="javascript:void(0)"
                                                        class="row align-items-center text-decoration-none text-dark open-profile-media"
                                                        data-menu="photo">
                                                        <div class="col-1">
                                                            <img src="{{ asset('img/icons/After/images-solid.svg') }}"
                                                                height="16" alt="icon">
                                                        </div>
                                                        <span class="align-middle col-11">Photo Files</span>
                                                    </a>
                                                </li>
                                                <li class="mb-1">
                                                    <a href="javascript:void(0)"
                                                        class="row align-items-center text-decoration-none text-dark open-profile-media"
                                                        data-menu="video">
                                                        <div class="col-1">
                                                            <img src="{{ asset('img/icons/After/film-solid.svg') }}"
                                                                height="16" alt="icon">
                                                        </div>
                                                        <span class="align-middle col-11">Video Files</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--/ User's Links -->
                                    </div>

                                    <div class="mt-2 p-4 bg-white">
                                        <!-- User's Links -->
                                        <div class="more-options">
                                            <h6 class="text-muted fw-bold mb-2">Actions</h6>
                                            <ul class="list-unstyled mb-0">
                                                <li class="mb-1">
                                                    <a href="javascript:void(0)"
                                                        class="d-flex align-items-center text-decoration-none text-danger delete-all-chats">
                                                        <i class="col-1 fa-solid fa-trash fs-6"></i>
                                                        <span class="align-middle">Delete All Messages</span>
                                                    </a>
                                                </li>
                                                <li class="mb-1">
                                                    @if (!$is_friend_blocked)
                                                        <a href="javascript:void(0)"
                                                            class="d-flex align-items-center text-decoration-none text-danger block-btn"
                                                            data-id="{{ $other_user->id }}">
                                                            <i class="col-1 fa-solid fa-ban fs-6"></i>
                                                            <span class="align-middle">Block User</span>
                                                        </a>
                                                    @else
                                                        <a href="javascript:void(0)"
                                                            class="d-flex align-items-center text-decoration-none text-success unblock-btn"
                                                            data-id="{{ $other_user->id }}">
                                                            <i class="col-1 fa-solid fa-lock-open fs-6"></i>
                                                            <span class="align-middle">Unblock User</span>
                                                    @endif
                                                </li>
                                                <li class="mb-1">
                                                    <a href="javascript:void(0)"
                                                        class="d-flex align-items-center text-decoration-none text-danger unfriend-btn"
                                                        data-id="{{ $other_user->id }}"
                                                        data-name="{{ $other_user->name }}">
                                                        <i class="col-1 fa-solid fa-user-minus fs-6"></i>
                                                        <span class="align-middle">Unfriend</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!--/ User's Links -->
                                    </div>
                                @else
                                    <div class="my-2 p-4 bg-white text-center">
                                        <!-- About User -->
                                        <h6 class="text-muted fw-bold mb-2">Sorry, this Account was no longer exist.
                                        </h6>
                                        <a href="javascript:void(0)"
                                            class="d-flex align-items-center justify-content-center text-decoration-none text-danger unfriend-btn"
                                            data-id="{{ $other_user->id }}" data-name="{{ $other_user->name }}">
                                            <i class="col-1 fa-solid fa-user-minus fs-6"></i>
                                            <span class="align-middle">Unfriend this Account.</span>
                                        </a>
                                        {{-- <h6 class="text-muted fw-bold mb-2">About</h6>
                                        @if (isset($other_user->bio))
                                            <p class="fs-6 text-dark mb-0">{{ "$other_user->bio" }}</p>
                                        @else
                                            <p class="fs-6 text-muted mb-0"><i>This user doesn't have bio</i></p>
                                        @endif --}}
                                        <!-- About User -->
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--/ Friend profile right area -->

                    <!-- Document media right area -->
                    <div class="d-none flex-column h-100 w-100 document-media" wire:ignore.self>
                        <div class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                            <div class="d-flex align-items-center w-100" style="height: 45px;">
                                <span class="btn btn-sm me-4 close-profile-media" style="cursor: pointer;"><i
                                        class="fa-solid fa-arrow-left text-dark  fs-5"></i></span>
                                <h4 class="chat-list-title mb-0">Document Files</h4>
                            </div>
                        </div>
                        <div class="w-100 custom-scrollbar" style="overflow-y: auto" data-scrollbar>
                            <div class="w-100">
                                @php
                                    $docs = [];
                                    foreach ($chats as $item) {
                                        if ($item->is_file && ($item->mime_type != 'png' && $item->mime_type != 'jpg' && $item->mime_type != 'mp4')) {
                                            array_push($docs, $item);
                                        }
                                    }
                                @endphp
                                @forelse ($docs as $item)
                                    @if ($item->$item->deleted_for_receiver == 1)
                                        @continue
                                    @else
                                        @if ($item->sender_id == $signed_user->id)
                                            @if ($item->deleted_from_sender == 1)
                                                @continue
                                            @else
                                                <a href="{{ asset("doc/chat/$item->content") }}"
                                                    download="{{ $item->content }}"
                                                    class="text-decoration-none text-dark mw-100"
                                                    data-bs-toggle="tooltip" data-bs-title="{{ $item->content }}">
                                                    <div class="py-2 px-3 m-2 d-flex align-items-center"
                                                        style="--bs-bg-opacity: .5;">
                                                        <i class="fa-solid fa-file-arrow-down fs-5"></i>
                                                        <div class="ps-3 mw-100">
                                                            <h6 class="mb-0 text-normal text-truncate pe-2">
                                                                {{ $item->content }}
                                                            </h6>
                                                            <p class="mb-0" style="font-size: .7rem;">
                                                                {{ $item->file_size >= 1000000 ? round($item->file_size / 1000000) . ' MB' : ($item->file_size >= 1000 ? round($item->file_size / 1000) . ' KB' : $item->file_size . ' Byte') }}
                                                                &nbsp;|&nbsp;
                                                                {{ strtoupper($item->mime_type) }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            @endif
                                        @else
                                            @if ($item->deleted_from_receiver == 1)
                                                @continue
                                            @else
                                                <a href="{{ asset("doc/chat/$item->content") }}"
                                                    download="{{ $item->content }}"
                                                    class="text-decoration-none text-dark mw-100"
                                                    data-bs-toggle="tooltip" data-bs-title="{{ $item->content }}">
                                                    <div class="py-2 px-3 m-2 d-flex align-items-center"
                                                        style="--bs-bg-opacity: .5;">
                                                        <i class="fa-solid fa-file-arrow-down fs-5"></i>
                                                        <div class="ps-3 mw-100">
                                                            <h6 class="mb-0 text-normal text-truncate pe-2">
                                                                {{ $item->content }}
                                                            </h6>
                                                            <p class="mb-0" style="font-size: .7rem;">
                                                                {{ $item->file_size >= 1000000 ? round($item->file_size / 1000000) . ' MB' : ($item->file_size >= 1000 ? round($item->file_size / 1000) . ' KB' : $item->file_size . ' Byte') }}
                                                                &nbsp;|&nbsp;
                                                                {{ strtoupper($item->mime_type) }}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </a>
                                            @endif
                                        @endif
                                    @endif
                                @empty
                                    <h6 class="py-5 text-dark text-center w-100">You doesn't have any document yet
                                    </h6>
                                @endforelse
                                {{-- <a href="{{ asset('img/1x1 padding.png') }}" download="proposed_file_name"
                                    class="text-decoration-none text-dark mw-100" data-bs-toggle="tooltip"
                                    data-bs-title="Coba aja ni.txt">
                                    <div class="py-2 px-3 m-2 d-flex align-items-center"
                                        style="--bs-bg-opacity: .5;">
                                        <i class="fa-solid fa-file-arrow-down fs-5"></i>
                                        <div class="ps-3 mw-100">
                                            <h6 class="mb-0 text-normal text-truncate pe-2">
                                                Coba aja ni asd asd asd asd asd asd asd asd asd asd asd asd.asdasdtxt
                                            </h6>
                                            <p class="mb-0" style="font-size: .7rem;">26 kB
                                                &nbsp;|&nbsp; TXT
                                            </p>
                                        </div>
                                    </div>
                                </a> --}}
                                {{-- <a href="{{ asset('img/1x1 padding.png') }}" download="proposed_file_name"
                                    class="text-decoration-none text-dark mw-100" data-bs-toggle="tooltip"
                                    data-bs-title="Coba aja ni.txt">
                                    <div class="py-2 px-3 m-2 d-flex align-items-center"
                                        style="--bs-bg-opacity: .5;">
                                        <i class="fa-solid fa-file-arrow-down fs-5"></i>
                                        <div class="ps-3 mw-100">
                                            <h6 class="mb-0 text-normal text-truncate pe-2">
                                                Coba aja ni.txt</h6>
                                            <p class="mb-0" style="font-size: .7rem;">26 kB
                                                &nbsp;|&nbsp; TXT
                                            </p>
                                        </div>
                                    </div>
                                </a> --}}
                                {{-- <a href="{{ asset('img/1x1 padding.png') }}" download="proposed_file_name"
                                    class="text-decoration-none text-dark mw-100" data-bs-toggle="tooltip"
                                    data-bs-title="Coba aja ni.txt">
                                    <div class="py-2 px-3 m-2 d-flex align-items-center"
                                        style="--bs-bg-opacity: .5;">
                                        <i class="fa-solid fa-file-arrow-down fs-5"></i>
                                        <div class="ps-3 mw-100">
                                            <h6 class="mb-0 text-normal text-truncate pe-2">
                                                Coba aja ni.txt</h6>
                                            <p class="mb-0" style="font-size: .7rem;">26 kB
                                                &nbsp;|&nbsp; TXT
                                            </p>
                                        </div>
                                    </div>
                                </a> --}}
                            </div>
                        </div>
                    </div>
                    <!--/ Document media right area -->

                    <!-- Photo media right area -->
                    <div class="d-none flex-column h-100 w-100 photo-media" wire:ignore.self>
                        <div class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                            <div class="d-flex align-items-center w-100" style="height: 45px;">
                                <span class="btn btn-sm me-4 close-profile-media" style="cursor: pointer;"><i
                                        class="fa-solid fa-arrow-left text-dark fs-5"></i></span>
                                <h4 class="chat-list-title mb-0">Photo Files</h4>
                            </div>
                        </div>
                        <div class="w-100 custom-scrollbar" style="overflow-y: auto; overflow-x: hidden;">
                            <div class="row p-1 g-1">
                                @php
                                    $imgs = [];
                                    foreach ($chats as $item) {
                                        if ($item->is_file && ($item->mime_type == 'png' || $item->mime_type == 'jpg')) {
                                            array_push($imgs, $item);
                                        }
                                    }
                                @endphp
                                @forelse ($imgs as $item)
                                    @if ($item->deleted_for_receiver == 1)
                                        @continue
                                    @else
                                        @if ($item->sender_id == $signed_user->id)
                                            @if ($item->deleted_from_sender == 1)
                                                @continue
                                            @else
                                                <div class="col-4">
                                                    <div class="position-relative w-100" style="padding-top: 100%;">
                                                        <img src="{{ asset("img/chat/$item->content") }}"
                                                            alt="img"
                                                            class="position-absolute top-50 start-50 translate-middle w-100 h-100 preview-img"
                                                            style="object-fit: cover; cursor: pointer;"
                                                            data-bs-target="#imageModal">
                                                    </div>
                                                </div>
                                            @endif
                                        @else
                                            @if ($item->deleted_from_receiver == 1)
                                                @continue
                                            @else
                                                <div class="col-4">
                                                    <div class="position-relative w-100" style="padding-top: 100%;">
                                                        <img src="{{ asset("img/chat/$item->content") }}"
                                                            alt="img"
                                                            class="position-absolute top-50 start-50 translate-middle w-100 h-100 preview-img"
                                                            style="object-fit: cover; cursor: pointer;"
                                                            data-bs-target="#imageModal">
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    @endif
                                @empty
                                    <h6 class="py-5 text-dark text-center w-100">You doesn't have any photo yet
                                    </h6>
                                @endforelse
                                {{-- <div class="col-4 p-1 overflow-hidden">
                                    <img src="{{ asset('img/user/Denas.png') }}" alt=""
                                        class="w-100 h-100 preview-img" style="object-fit: cover; cursor: pointer;"
                                        data-bs-target="#imageModal">
                                </div> --}}
                                {{-- <div class="col-4 p-1 overflow-hidden">
                                    <img src="{{ asset('img/user/Denas.png') }}" alt=""
                                        class="w-100 h-100 preview-img" style="object-fit: cover; cursor: pointer;"
                                        data-bs-target="#imageModal">
                                </div> --}}
                                {{-- <div class="col-4 p-1 overflow-hidden">
                                    <img src="{{ asset('img/user/Denas.png') }}" alt=""
                                        class="w-100 h-100 preview-img" style="object-fit: cover; cursor: pointer;"
                                        data-bs-target="#imageModal">
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <!--/ Photo media right area -->

                    <!-- Video media right area -->
                    <div class="d-none flex-column h-100 w-100 video-media" wire:ignore.self>
                        <div class="px-4 py-3 border-start border-white border-2 bg-light2 sticky-top shadow-sm">
                            <div class="d-flex align-items-center w-100" style="height: 45px;">
                                <span class="btn btn-sm me-4 close-profile-media" style="cursor: pointer;"><i
                                        class="fa-solid fa-arrow-left text-dark fs-5"></i></span>
                                <h4 class="chat-list-title mb-0">Video Files</h4>
                            </div>
                        </div>
                        <div class="w-100 custom-scrollbar" style="overflow-y: auto">
                            <div class="d-flex flex-wrap w-100">
                                @php
                                    $vids = [];
                                    foreach ($chats as $item) {
                                        if ($item->is_file && $item->mime_type == 'mp4') {
                                            array_push($vids, $item);
                                        }
                                    }
                                @endphp
                                @forelse ($vids as $item)
                                    @if ($item->$item->deleted_for_receiver == 1)
                                        @continue
                                    @else
                                        @if ($item->sender_id == $signed_user->id)
                                            @if ($item->deleted_from_sender == 1)
                                                @continue
                                            @else
                                                <div class="col-4 p-1 overflow-hidden">
                                                    <video class="w-100 h-100 preview-vid"
                                                        style="object-fit: cover; cursor: pointer;"
                                                        data-bs-target="#videoModal" nocontrols>
                                                        <source src="{{ asset("vid/chat/$item->content") }}">
                                                        Your browser does not support HTML video.
                                                    </video>
                                                </div>
                                            @endif
                                        @else
                                            @if ($item->deleted_from_receiver == 1)
                                                @continue
                                            @else
                                                <div class="col-4 p-1 overflow-hidden">
                                                    <video class="w-100 h-100 preview-vid"
                                                        style="object-fit: cover; cursor: pointer;"
                                                        data-bs-target="#videoModal" nocontrols>
                                                        <source src="{{ asset("vid/chat/$item->content") }}">
                                                        Your browser does not support HTML video.
                                                    </video>
                                                </div>
                                            @endif
                                        @endif
                                    @endif
                                @empty
                                    <h6 class="py-5 text-dark text-center w-100">You doesn't have any video yet
                                    </h6>
                                @endforelse
                                {{-- <div class="col-4 p-1 overflow-hidden">
                                    <video class="w-100 h-100 preview-vid"
                                        style="object-fit: cover; cursor: pointer;" data-bs-target="#videoModal"
                                        nocontrols>
                                        <source src="{{ asset('vid/vid1.mp4') }}">
                                        Your browser does not support HTML video.
                                    </video>
                                </div> --}}
                                {{-- <div class="col-4 p-1 overflow-hidden">
                                    <video class="w-100 h-100 preview-vid"
                                        style="object-fit: cover; cursor: pointer;" data-bs-target="#videoModal"
                                        nocontrols>
                                        <source src="{{ asset('vid/vid2.mp4') }}">
                                        Your browser does not support HTML video.
                                    </video>
                                </div> --}}
                                {{-- <div class="col-4 p-1 overflow-hidden">
                                    <video class="w-100 h-100 preview-vid"
                                        style="object-fit: cover; cursor: pointer;" data-bs-target="#videoModal"
                                        nocontrols>
                                        <source src="{{ asset('vid/vid1.mp4') }}">
                                        Your browser does not support HTML video.
                                    </video>
                                </div> --}}
                                {{-- <div class="col-4 p-1 overflow-hidden">
                                    <video class="w-100 h-100 preview-vid"
                                        style="object-fit: cover; cursor: pointer;" data-bs-target="#videoModal"
                                        nocontrols>
                                        <source src="{{ asset('vid/vid2.mp4') }}">
                                        Your browser does not support HTML video.
                                    </video>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <!--/ Video media right area -->

                </div>
                <!--/ Friend profile right area -->
            @endif
        @endif
        {{-- End right side area --}}
    </div>
</div>

@push('script')
    <script wire.ignore>
        $(document).ready(function() {
            // let user_email;
            // Livewire.on("initJsEmailUserData", () => {
            //     user_email = '{{ $this->signed_user->email }}';
            // });
            // console.log(user_email);

            // Start livewire event close modal for edit profile
            Livewire.on("closeEditProfileModal", () => {
                $('#editProfileModal').modal('hide');
            });
            // End livewire close modal for edit profile

            // Start livewire event close modal for edit profile
            Livewire.on("closeChangePasswordModal", () => {
                $('#changePasswordModal').modal('hide');
            });
            // End livewire close modal for edit profile

            // Start giving value darkmode from js to php livewire
            let darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                "darkSwitch") === "dark";
            if (darkThemeSelected) {
                @this.darkMode(1);
            } else {
                @this.darkMode(0);
            }
            Livewire.on("darkModeInit", () => {
                darkThemeSelected = localStorage.getItem("darkSwitch") !== null && localStorage.getItem(
                    "darkSwitch") === "dark";
                if (darkThemeSelected) {
                    @this.darkMode(1);
                } else {
                    @this.darkMode(0);
                }
            });
            // End giving value darkmode from js to php livewire

            // Start preview modal when image clicked
            const imageModal = document.getElementById('imageModal')
            $('body').on('click', '.preview-img', function(event) {
                let button = event.currentTarget
                let content = button.getAttribute('src')
                console.log(content)
                imageModal.getElementsByTagName("img")[0].src = content
                console.log(imageModal.getElementsByTagName("img")[0])
                const openImageModal = new bootstrap.Modal('#imageModal', {
                    keyboard: false
                })
                openImageModal.show();
            })
            // End preview modal when image clicked

            // Start preview modal when video clicked
            const videoModal = document.getElementById('videoModal')
            $('body').on('click', '.preview-vid', function(event) {
                let button = event.currentTarget
                let content = button.querySelector('source').getAttribute('src')
                console.log(content)
                const modalBodyVideo = videoModal.querySelector('.modal-body video')
                modalBodyVideo.querySelector('source').src = content
                modalBodyVideo.load()
                const openVideoModal = new bootstrap.Modal('#videoModal', {
                    keyboard: false
                })
                openVideoModal.show()
            })
            // End preview modal when video clicked

            // Start SWAL when unfriend btn clicked
            $('body').on('click', '.unfriend-btn', function() {
                const name = $(this).data('name');
                const id = $(this).data('id');
                const type = $(this).data('type');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "Are you sure you want to unfriend with " + name + "?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Unfriend!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        if (type == 'leave-chat-room') {
                            @this.deleteChatRoom(id);
                        } else {
                            @this.unFriend(id);
                        }
                    }
                })
            });
            // End SWAL when unfriend btn clicked

            // Start SWAL when block btn clicked
            $('body').on('click', '.block-btn', function() {
                const id = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "Are you sure you want to block this person?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, block it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.blockFriend(id);
                    }
                })
            });
            // End SWAL when block btn clicked

            // Start SWAL when unblock btn clicked
            $('body').on('click', '.unblock-btn', function() {
                const id = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure?',
                    text: "Are you sure you want to unblock this person?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, unblock it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.unblockFriend(id);
                    }
                })
            });
            // End SWAL when block btn clicked

            // Start SWAL when delete account btn clicked
            $('body').on('click', '.delete-account-btn', function() {
                Swal.fire({
                    title: 'Are you sure you want to delete your account?',
                    text: "Deleted data cannot be recovered (including chat history data and friends). Your friend will also not be able to chat with you again!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.deleteAccount();
                    }
                })
            });
            // End SWAL when delete account btn clicked

            // Start SWAL when unsend chat btn clicked
            $('body').on('click', '.unsend-msg', function() {
                const id = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure',
                    text: "Want to unsend this chat? unsended chat data cannot readed anymore!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.unsendChat(id);
                    }
                });
            });
            // End SWAL when unsend chat btn clicked

            // Start SWAL when delete chat btn clicked
            $('body').on('click', '.delete-msg', function() {
                const id = $(this).data('id');

                Swal.fire({
                    title: 'Are you sure',
                    text: "Want to delete this chat? Deleted data cannot be recovered!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.deleteChat(id);
                    }
                });
            });
            // End SWAL when delete chat btn clicked

            // Start SWAL when clear all chat btn clicked
            $('body').on('click', '.delete-all-chats', function() {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Want to delete all chat history with this user? Deleted data cannot be recovered!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya, Delete It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.deleteAllChat();
                    }
                })
            });
            // End SWAL when clear all chat btn clicked

            // Start open search user form
            $('body').on('click', '.open-search', function(event) {
                const button = event.currentTarget
                button.parentElement.style.width = '0px';
                button.parentElement.style.height = '0px';
                button.parentElement.style.transition = 'width .3s';
                button.parentElement.nextElementSibling.style.width = '100%'
                button.parentElement.nextElementSibling.style.height = 'auto'
                button.parentElement.nextElementSibling.style.transition = 'width .3s';
            });
            // End open search user form

            // Start close search user form
            $('body').on('click', '.close-search', function(event) {
                const button = event.currentTarget
                button.parentElement.parentElement.style.width = '0px';
                button.parentElement.parentElement.style.height = '0px';
                button.parentElement.parentElement.style.transition = 'width .3s';
                button.parentElement.parentElement.previousElementSibling.style.width = '100%';
                button.parentElement.parentElement.previousElementSibling.style.height = 'auto';
                button.parentElement.parentElement.previousElementSibling.style.transition = 'width .3s';
            });
            // End close search user form

            // Start open live input edit user data form  
            $('body').on('click', '.open-live-input', function(event) {
                const button = event.currentTarget
                button.parentElement.style.width = '0px';
                button.parentElement.style.height = '0px';
                button.parentElement.style.transition = 'width .3s';
                button.parentElement.nextElementSibling.style.width = '100%'
                button.parentElement.nextElementSibling.style.height = 'auto'
                button.parentElement.nextElementSibling.style.transition = 'width .3s';
                button.parentElement.nextElementSibling.getElementsByTagName("INPUT")[0].focus();
            });
            // End open live input edit user data form

            // Start close live input edit user data form
            $('body').on('click', '.close-live-input', function(event) {
                const button = event.currentTarget
                button.parentElement.style.width = '0px';
                button.parentElement.style.height = '0px';
                button.parentElement.style.transition = 'width .3s';
                button.parentElement.getElementsByTagName("INPUT")[0].value = '';
                button.parentElement.previousElementSibling.style.width = '100%';
                button.parentElement.previousElementSibling.style.height = 'auto';
                button.parentElement.previousElementSibling.style.transition = 'width .3s';
            });
            // Start close live input edit user data form

            // Start preview img edit profile
            const image_extension = ['png', 'jpg', 'jpeg', 'webp', 'gif', 'svg'];
            $('#photoInput').on("change", function(event) {
                let element = $(this).closest('.modal');
                let file = element.find('#photoInput')[0].files[0];
                let fileName = element.find('#photoInput')[0].value;
                let extension = fileName.substring(fileName.lastIndexOf('.') + 1);
                let reader = new FileReader();

                let validation = image_extension.findIndex(element => {
                    if (element.toLowerCase().includes(extension.toLowerCase())) {
                        return true;
                    }
                });

                if (validation !== -1) {
                    reader.onloadend = function() {
                        element.find('.preview-img-profile').attr("src", reader.result);
                    }

                    if (file) {
                        reader.readAsDataURL(file);
                    }
                }
            });
            // End preview img edit profile

            // Start reset photo profile button
            $('body').on('click', '#resetPhoto', function() {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Want to remove your current photo? your photo will change to default again!!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, Remove It!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        @this.removePhotoProfile();
                        $('#editProfileModal').find('.preview-img-profile').attr("src",
                            "../img/user/user.png");
                    }
                })
            });
            // End reset photo profile button

            // Start refresh preview img user profile in edit profile modal 
            Livewire.on("changePreviewPhotoProfile", () => {
                $('#editProfileModal').find('.preview-img-profile').attr("src", @this.signed_user.photo);
            });
            // End refresh preview img user profile in edit profile modal 

            // Start chat modal when viewport mobile
            const mobileWidthLimit = 992;
            let windowWidth = window.innerWidth;
            $('body').on('click', '.open-chat', function(event) {
                let button = event.currentTarget
                let room_id = button.getAttribute('data-room-id')
                let user_id = button.getAttribute('data-user-id')
                if (room_id) {
                    @this.openRoom(room_id, user_id).then(function() {
                        if (windowWidth <= mobileWidthLimit) {
                            console.log('openMobileChat');
                            let openChatModal = new bootstrap.Modal('#activeChatMobile', {
                                keyboard: false
                            })
                            openChatModal.show();
                        }
                    })
                } else {
                    @this.openRoomIfExist(user_id).then(function() {
                        room_id = "@this.room ? @this.room->id : ''"
                        if (!room_id) {
                            $('.open-profile').click()
                        } else {
                            if (windowWidth <= mobileWidthLimit) {
                                console.log('openMobileChat');
                                let openChatModal = new bootstrap.Modal('#activeChatMobile', {
                                    keyboard: false
                                })
                                openChatModal.show();
                            }
                        }
                    })
                }
            })
            // End preview modal when video clicked

            // Start open friend profile or other profile section
            $('body').on('click', '.open-profile', function() {
                const left_area = $('body').find('.left-area');
                left_area.addClass("d-xxl-block");
                if ((windowWidth <= mobileWidthLimit) && @this.room) {
                    console.log('openMobileProfile');
                    const openProfileModal = new bootstrap.Modal('#detailProfileMobile', {
                        keyboard: false
                    })
                    openProfileModal.show();
                }
            });
            // End open friend profile or other profile section

            // Start close friend profile or other profile section
            $('body').on('click', '.close-profile', function() {
                const left_area = $('body').find('.left-area');
                left_area.removeClass("d-xxl-block");
            });
            // End close friend profile or other profile section

            // Start open list media chat on friend profile section
            $('body').on('click', '.open-profile-media', function(event) {
                const button = event.currentTarget;
                const menu = button.getAttribute('data-menu');
                if (menu == 'document') {
                    const detail_profile = $('body').find('.detail-profile');
                    detail_profile.removeClass("d-flex");
                    detail_profile.addClass("d-none");

                    const document_media = $('body').find('.document-media');
                    document_media.removeClass("d-none");
                    document_media.addClass("d-flex");
                } else if (menu == 'photo') {
                    const detail_profile = $('body').find('.detail-profile');
                    detail_profile.removeClass("d-flex");
                    detail_profile.addClass("d-none");

                    const photo_media = $('body').find('.photo-media');
                    photo_media.removeClass("d-none");
                    photo_media.addClass("d-flex");
                } else if (menu == 'video') {
                    const detail_profile = $('body').find('.detail-profile');
                    detail_profile.removeClass("d-flex");
                    detail_profile.addClass("d-none");

                    const video_media = $('body').find('.video-media');
                    video_media.removeClass("d-none");
                    video_media.addClass("d-flex");
                } else {
                    alert('no menu found!');
                }
            });
            // End open list media chat on friend profile section

            // Start close list media chat on friend profile section
            $('body').on('click', '.close-profile-media', function(event) {
                const document_media = $('body').find('.document-media');
                document_media.removeClass("d-flex");
                document_media.addClass("d-none");

                const photo_media = $('body').find('.photo-media');
                photo_media.removeClass("d-flex");
                photo_media.addClass("d-none");

                const video_media = $('body').find('.video-media');
                video_media.removeClass("d-flex");
                video_media.addClass("d-none");

                const detail_profile = $('body').find('.detail-profile');
                detail_profile.removeClass("d-none");
                detail_profile.addClass("d-flex");
            });
            // End close list media chat on friend profile section
        });
    </script>

    {{-- <script>
        let user_email;
        document.addEventListener("DOMContentLoaded", () => {
            Livewire.hook('message.sent', (message, component) => {
                user_email = @js(auth()->user()->email);
                console.log('user1: ' + user_email);
            });

            console.log('check');
            // Start check if email edited in edit profile
            $('body').on('click', '#submitEditProfile', function(event) {
                let current_email = @js(auth()->user()->email);
                const new_email = event.currentTarget.parentElement.parentElement.querySelector(
                    "#floatingInputEmail").value;
                console.log('user2: ' + user_email);
                console.log('current: ' + current_email);
                console.log('new: ' + new_email);
                if (current_email == new_email) {
                    @this.editProfile();
                } else {
                    // Swal.fire({
                    //     title: 'Are you sure?',
                    //     text: "Want to change your current email with a new email? you need to re-verify your email before you can access this app again!!",
                    //     icon: 'warning',
                    //     showCancelButton: true,
                    //     confirmButtonColor: '#3085d6',
                    //     cancelButtonColor: '#d33',
                    //     confirmButtonText: 'Ya, Change It!'
                    // }).then((result) => {
                    //     if (result.isConfirmed) {
                    //         @this.editProfile();
                    //     }
                    // });
					@this.editEmailConfirm();
                }
            });
            // End check if email edited in edit profile
        });
    </script> --}}
@endpush
