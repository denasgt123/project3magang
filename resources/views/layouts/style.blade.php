<!-- Fonts -->
{{-- <link rel="dns-prefetch" href="//fonts.gstatic.com"> --}}
{{-- <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet"> --}}

<!-- Scripts -->
{{-- @vite(['resources/sass/app.scss', 'resources/js/app.js']) --}}

<!-- Bootstrap Style CDN -->
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous"> --}}
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

{{-- Font Awesome kit CDN --}}
{{-- <script src="https://kit.fontawesome.com/80a5c9094f.js" crossorigin="anonymous"></script> --}}
<script src="{{ asset('js/fontawesome/80a5c9094f.js') }}" crossorigin="anonymous"></script>

{{-- AOS Animation CDN --}}
{{-- <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"> --}}
<link href="{{ asset('css/aos.css') }}" rel="stylesheet">

{{-- Darkmode CSS --}}
<link href="{{ asset('css/darkmode.css') }}" rel="stylesheet">

<!-- PWA  -->
<meta name="theme-color" content="#6777ef" />
<link rel="apple-touch-icon" href="{{ asset('img/logo.png') }}">
<link rel="manifest" href="{{ asset('/manifest.json') }}">

<!-- PWA Chrome Android  -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="application-name" content="HCT">
<link rel="icon" sizes="512x512" href="{{ asset('img/logo.png') }}">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-ba+r-style" content="black">
<meta name="apple-mobile-web-app-title" content="ACT">

{{-- Style --}}
{{-- <link href="{{ asset('css/stylehome.css') }}" rel="stylesheet"> --}}
<link href="{{ asset('css/style.css') }}" rel="stylesheet">