<?php

use App\Http\Controllers\HomeController;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Auth\Register;
use App\Http\Livewire\FillData;
use App\Http\Livewire\Home;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
// 	return view('welcome');
// });

Route::get('/', function () {
	return redirect()->route('login');
});

Auth::routes(['login' => false, 'register' => false, 'verify' => true]);

Route::get('/google/auth/redirect', [Login::class, 'loginGoogleRedirect'])->name('google');
Route::get('/google/auth/callback', [Login::class, 'loginGoogleCallback']);

Route::middleware('guest')->group(function () {
	Route::get('/login', Login::class)->name('login');
	Route::get('/register', Register::class)->name('register');
});

Route::middleware('auth')->group(function () {
	Route::get('/fill-data', FillData::class)->name('fill-data');
	Route::get('/home', Home::class)->middleware(['auth', 'verified'])->name('home');
	Route::get('/verify-edit', function () {
		return view('auth.verify-edit');
	});
});

// Route::get('/home', [HomeController::class, 'index'])->name('home');
// Route::get('/home', Home::class)->middleware(['auth', 'verified'])->name('home');
Route::get('/chat', function () {
	return view('livewire.chat');
});
