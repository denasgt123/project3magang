<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    use HasFactory;

    protected $guarded = [];
    
    /**
     * Get all of the chats for the ChatRoom
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chats()
    {
        return $this->hasMany(Chat::class, 'room_id', 'id');
    }

	/**
	 * Get the latestChat associated with the ChatRoom
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function latestChat()
	{
		return $this->hasOne(Chat::class, 'room_id', 'id')->latest();
	}

    /**
     * Get all of the participants for the ChatRoom
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function participants()
    {
        return $this->hasMany(Participant::class, 'room_id', 'id');
    }

    /**
     * The users that belong to the ChatRoom
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'participants', 'room_id', 'user_id');
    }

    public function scopeHasUser($query, $user_id)
    {
        return $query->whereHas('participants', function ($q) use ($user_id) {
            $q->whereUserId($user_id);
        });
    }
}
